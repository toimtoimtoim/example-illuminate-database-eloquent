# Cross-items example with Eloquent ORM (illuminate/database/Eloquent) 

This is a proof of concept implementation of cross-items (our company internal custom made ORM + mapper library) functionality with Illuminate database packages

It is NOT meant to be a mapper or an ORM!
It is meant to fetch data from DB to specific format (api response structure) by using features provided by Eloquent ORM/Query builder.
It provides abstract service and controller that can be extended to suit different requirements

## Requirements / Goals:

- Business entities should be defined as models and support relations ala deal has one person, organization. activity has many participating persons
    - [x] metadata definition for models must be minimal
    - [x] virtual field support and be included in rest API response
      - [x] raw virtual columns (i.e. select max(a.id) as last_actitvity_id )
      - [x] some other table column reference virtual columns (i.e. persons.name on deals model as column "person_name" )
    - [x] field mutators when outputting ala date fields are converted to some timezone or are formatted differently
        * https://laravel.com/docs/5.4/eloquent-mutators#introduction
        * https://laravel.com/docs/5.4/eloquent-mutators#date-mutators
        * https://laravel.com/docs/5.4/eloquent-mutators#attribute-casting


- rest endpoints should support:
    - [x] partial responses i.e. defining return entities fields by "id,name,person(id,name,email(primary)),org(id,name)"
      - [x] partial response should support at least 3 levels of nesting (ala for deal "id,person(id,email(id,primary))")
      - [x] when relation is mentioned in partial request without parentheses, all that relation fields must be fetched ("select *")
    - [x] filtering by string / filter id
    - [x] pagination
    - [x] extraction of related objects in result collection


- Service to fetch entity collection with filters etc and it should contain related entities data
    - [x] n+1 query problem should be addressed. one-to-one relations should be fetched by joins, 
    - [x] one-to-many relations by single query
    - [x] many-to-many (pivot table) relations by single query
    - [x] entity structure hydration should be efficient (compared to what?) minimal amount of work
    - [x] hydration should support at least 2 level ala Deal->Person
    - [x] hydration should support at least 3 level ala Deal->Person->Pictures
    - [x] fetched data structure i.e. included related entities should depend on fields mentioned in partial response fields
    - [x] entities visibility/permissions should be addressed automatically (difference between one-to-one and one-to-many relation)


- Filtering should be dynamic - give filterId and needed criteria for query is built
    - [ ] filtering should support multitude of operator (=,!=,LIKE, NOT LIKE, IS NULL, >,<,>= etc)
    - [x] filters are applied on one-to-one (added as ordinary where clause) relation and one-to-many (possibly left join with double query)
    - [x] filter by text
    - [x] filter by id
    - [x] filters should be loaded from database
    - [ ] filters should be stored in database


- Performance benchmarking:
    - [ ] sql scripts to bulk generate data (1 000 000+ entities)
    - [ ] rest endpoint benchmark suite to measure request per sec
    - [ ] unittest etc to benchmark fetching 10 000 entities 1000 times (mock ResultSet to benchmark entity structure hydration)
    - [ ] measure memory usage


## Pros/Cons compared to Crossitems:

### Pro:

1. fields/relation definition done with dedicated classes - so easier define and to understand why and where something is used
1. field/relation definition behavior is easily findable from model class - in crossitems definition is in meta class but
    behaviour is for one-to-many relation in Aggregator/Extractor/Filler etc classes
1. does not contain domain logic vs. crossitems core classes are full of our company related logic (deals/org/person). So this
    is more generic solution
1. takes care of one-to-many eager loading with Eloquent eager loading https://laravel.com/docs/5.5/eloquent-relationships#eager-loading
1. services/metas are stateless - so one service can be called multiple times with different params during one PHP execution
    and there is problem with static stuff from first call would mess things whit second call
1. Eloquent has very strong community, documentation and rich feature set - PD ORM has none of those

### Cons:

1. Current implementation is mostly inheritance (abstract service/controller) but CrossItems is Composition (criteria classes)
1. probably not that memory efficient as crossitems i.e. Eloquent models contain by default orginal field values as internal array
    but this can be overridden with custom base model that does NoOp on this logic
    
### Equally missing:

1. does not provide separation of Meta/Model (DB) from API structure - so v1 has field 'x' but v2 does not is not easily achievable


## Requirements
* PHP 7.1+
    * mod_pdo
    * mod_pdo_mysql
* Composer [https://getcomposer.org]
* Docker [https://www.docker.com]

## Setup for development

Quick install (composer and docker-compose globally installed)
```
composer install
docker-compose up
vendor/bin/phinx migrate
vendor/bin/phinx seed:run
touch logs/app.log && chmod 777 logs/app.log
```

Install dependencies with composer

    composer install
   
Start dockerized MySql (exposes localhost:33060 port if you need to connect with client)

    docker-compose up
    
open [http://172.17.0.1:8088/deal?API_TOKEN=TOKEN_FOR_LOCAL_TEST&filter_id=1&fields=id,person_id,person(id,name),organization(id),user_id] in browser to verify that env started successfully
NB: 'http://172.17.0.1' is docker IP. This may be different for you. Check with 'ifconfig'/'ipconfig' commands

Migrate database to initial state and seed with initial data

    vendor/bin/phinx migrate
    vendor/bin/phinx seed:run

Open [http://172.17.0.1:8088/deal?API_TOKEN=TOKEN_FOR_LOCAL_TEST] in browser

## remote debugging with xdebug

replace `xdebug.remote_host=` value in `docker/php/20-xdebug.ini` to ip that Virtualbox host network has
and re-build docker images `docker-compose rm && docker-compose build && docker-compose up`

## Tests

Run all test suites (unit and integration tests)
```
composer test
```

Run only unit tests
```
composer test-unit
```
    
Run only integrations tests. Needs database to be up. So start docker before!   
```
composer test-integration
```

Run specific integration test with xdebug from command line
```
export XDEBUG_CONFIG="idekey=PHPSTORM remote_host=localhost remote_enable=1"
/vendor/bin/phpunit --filter 'Integration\\App\\Domain\\Deal\\DealListServiceTest::testDealListPartialResponse$'
```
    
## SQL query logging

Add environment variable "ENV=DEV" and all sql queries will be logged into "./logs/app.log" file