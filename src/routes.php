<?php

$app->get('/deal', App\Domain\Deal\DealListController::class . ':index');
$app->get('/activity', App\Domain\Activity\ActivityListController::class . ':index');
$app->get('/person', App\Domain\Person\PersonListController::class . ':index');
