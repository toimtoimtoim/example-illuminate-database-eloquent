<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'timezone' => 'UTC', // lets assume we are at UTC always
        'logger' => [
            'name' => 'app',
            'path' => env('APP_LOG_PATH', __DIR__ . '/../logs/app.log'),
            'level' => constant('\Monolog\Logger::' . strtoupper(env('APP_LOG_LEVEL', 'INFO'))),
        ],
        'db' => [
            'driver' => 'mysql',
            'host' => 'db',
            'port' => '3306',
            'database' => 'company',
            'username' => 'root',
            'password' => 'secret',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
    ],
];
