<?php

use App\Constants;
use App\Database\CustomMySqlConnection;
use App\Domain\Activity\ActivityListController;
use App\Domain\Activity\ActivityListService;
use App\Domain\Deal\DealListController;
use App\Domain\Deal\DealListService;
use App\Domain\Filter\FilterService;
use App\Domain\Person\PersonListController;
use App\Domain\Person\PersonListService;
use App\Domain\User\UserService;
use App\Field\FieldService;
use App\Field\FieldValidatorService;
use App\Filter\FilterNodesService;
use App\Filter\Parser\SqlBuilderGrammar;
use App\Filter\Parser\SqlLexer;
use Illuminate\Database\Connection;

$container = $app->getContainer();

$container[Constants::DB_INSTANCE] = function ($container) {
    $dbSettings = $container['settings'][Constants::DB_INSTANCE];
    $capsule = new \Illuminate\Database\Capsule\Manager;

    // register custom resolver for mysql to have our own resultset hydration
    Connection::resolverFor('mysql', function ($connection, $database, $prefix, $config) {
        return new CustomMySqlConnection($connection, $database, $prefix, $config);
    });

    $capsule->addConnection($dbSettings);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    // set timezone for timestamps etc
    date_default_timezone_set($container['settings']['timezone']);

    return $capsule;
};

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];

    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());

    // probably should use SyslogUdpHandler or GelfHandler
    // see possibilities: https://github.com/Seldaek/monolog/blob/master/doc/02-handlers-formatters-processors.md#log-specific-servers-and-networked-logging
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    // Register the logger to handle PHP errors and exceptions
    Monolog\ErrorHandler::register($logger);

    return $logger;
};

$container['errorHandler'] = function ($c) {
    /** @var Monolog\Logger $logger */
    $logger = $c['logger'];
    return function ($request, $response, $exception) use ($c, $logger) {
        if ($exception instanceof \App\AppValidationException) {
            return $c['response']->withJson(
                ['messages' => [$exception->getMessage()]],
                422
            );
        } else {
            $logger->error('errorHandler:: '
                . $exception->getMessage()
                . ' Stack: '
                . $exception->getTraceAsString()
            );

            return $c['response']->withJson(
                ['messages' => ['TECHNICAL_ERROR']],
                500
            );
        }
    };
};

$container[DealListService::class] = function ($c) {
    return new DealListService(
        $c->get(Constants::DB_INSTANCE),
        $c->get(FilterService::class),
        $c->get('logger')
    );
};

$container[ActivityListService::class] = function ($c) {
    return new ActivityListService(
        $c->get(Constants::DB_INSTANCE),
        $c->get(FilterService::class),
        $c->get('logger')
    );
};

$container[PersonListService::class] = function ($c) {
    return new PersonListService(
        $c->get(Constants::DB_INSTANCE),
        $c->get(FilterService::class),
        $c->get('logger')
    );
};

$container[FilterNodesService::class] = function () {
    return new FilterNodesService(new SqlLexer(), new SqlBuilderGrammar());
};

$container[FieldService::class] = function ($c) {
    return new FieldService($c->get(Constants::DB_INSTANCE));
};

$container[FieldValidatorService::class] = function ($c) {
    return new FieldValidatorService($c->get(FieldService::class));
};

$container[UserService::class] = function () {
    return new UserService();
};

$container[FilterService::class] = function () {
    return new FilterService();
};

$container->get(Constants::DB_INSTANCE); // eager load so eloquent would be bootstrapped


// controllers
$container[ActivityListController::class] = function ($c) {
    return new ActivityListController(
        $c->get(FilterNodesService::class),
        $c->get(FieldValidatorService::class),
        $c->get(UserService::class),
        $c->get(ActivityListService::class)
    );
};

$container[DealListController::class] = function ($c) {
    return new DealListController(
        $c->get(FilterNodesService::class),
        $c->get(FieldValidatorService::class),
        $c->get(UserService::class),
        $c->get(DealListService::class)
    );
};

$container[PersonListController::class] = function ($c) {
    return new PersonListController(
        $c->get(FilterNodesService::class),
        $c->get(FieldValidatorService::class),
        $c->get(UserService::class),
        $c->get(PersonListService::class)
    );
};
