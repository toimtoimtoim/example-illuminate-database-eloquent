<?php

namespace App\Database;

use App\Database\Column\SelectColumnUtils;
use App\Database\Relations\Relation;
use App\Domain\User\UserModel;
use App\Field\Partial\EntityFields;
use Illuminate\Database\Eloquent\Collection;

/**
 * EagerLoad is abstraction for one-to-many relations fetching. Fetching is done after main query has been executed
 *
 * EagerLoad is information about 1 relation identified by field name that is to be eagerly loaded to (main) entity instances
 * takes care of N+1 problem
 */
class EagerLoad
{
    /**
     * @var string
     */
    private $fieldsParentPath;

    /**
     * @var EntityFields
     */
    private $fields;

    /**
     * @var Relation
     */
    private $relation;

    public function __construct(Relation $relation, $fieldsParentPath, ?EntityFields $fields)
    {
        $this->relation = $relation;
        $this->fieldsParentPath = $fieldsParentPath;
        $this->fields = $fields;
    }

    public function loadToModels(Collection $models, ?UserModel $user)
    {
        $currentModel = $this->relation->getModelInstance();
        $fieldName = $this->relation->getFieldName();

        if ($this->fields) {
            $selectedColumns = SelectColumnUtils::extractColumnsForSelect($fieldName, $this->fields->getFields(), $currentModel::getVirtualColumns());
        } else {
            $selectedColumns = null;
        }

        $relation = $this->relation;
        $models->load([$fieldName => function ($query) use ($selectedColumns, $relation, $user) {
            // when loading relations we need to apply permissions and explicitly set fields that we cant to fetch from db
            // so do it all within this callback

            $queryContext = new QueryContext($query, $user);
            $permission = $relation->getPermission();

            if ($selectedColumns) {
                if ($relation->isExtractable()) {
                    // When relation is extracted and its is a partial request we need to have primary key field also selected
                    // because it is used as a (unique) key in related objects collection
                    $query->addSelect($relation->getQualifiedLocalKeyName());
                }
                $query->addSelect($query->getQualifiedForeignKeyName()); // FK is mandatory for partial response so eager loaded model could be matched back to parent model
                SelectColumnUtils::applyColumnsToQuery($selectedColumns, $queryContext);
            }

            if ($permission) {
                $permission->apply($queryContext);
            }
        }]);
    }

    /**
     * @return string
     */
    public function getFieldsParentPath(): ?string
    {
        return $this->fieldsParentPath;
    }

    public function getRelationFieldName(): string
    {
        return $this->relation->getFieldName();
    }

}