<?php

namespace App\Database;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

final class QueryUtils
{
    private function __construct()
    {
        // utils class
    }

    /**
     * Check if table is already joined in query.
     *
     * NB: relations are joined with aliases ala "deals as deal"
     *
     * @param Builder $query
     * @param $table
     * @return bool
     */
    public static function isTableJoined(Builder $query, $table): bool
    {
        $joins = $query->getQuery()->joins;
        if (!$joins) {
            return false;
        }

        $joinedTables = array_map(function (JoinClause $joinClause) {
            return $joinClause->table;
        }, $joins);

        return in_array($table, $joinedTables, true);
    }

    /**
     * Check if column is already included in query selectable columns list
     *
     * @param Builder $query
     * @param $column
     * @return bool
     */
    public static function isColumnSelected(Builder $query, $column): bool
    {
        $columns = $query->getQuery()->columns;
        if (!$columns) {
            return false;
        }
        return in_array($column, $columns, true);
    }

}