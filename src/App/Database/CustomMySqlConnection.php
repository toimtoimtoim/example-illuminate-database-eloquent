<?php

namespace App\Database;

use App\Database\Column\VirtualColumn;
use Illuminate\Database\MySqlConnection;
use PDO;

class CustomMySqlConnection extends MySqlConnection
{

    private $useCustomMapping = false;

    /**
     * @var string
     */
    private $rootModel;

    /**
     * Run a select statement against the database.
     *
     * @param  string $query
     * @param  array $bindings
     * @param  bool $useReadPdo
     * @return array
     * @throws \Illuminate\Database\QueryException
     */
    public function select($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }

            if ($this->useCustomMapping) {
                /**
                 * NB: PDO::FETCH_NUM is essential or otherwise joined tables columns with same name are merged as SINGLE key in array
                 */
                $this->fetchMode = PDO::FETCH_NUM;
            }

            $statement = $this->prepared($this->getPdoForSelect($useReadPdo)
                ->prepare($query));

            $this->bindValues($statement, $this->prepareBindings($bindings));

            $statement->execute();

            // --------- start of custom part ---------
            if (!$this->useCustomMapping) {
                return $statement->fetchAll();
            }

            try {
                $columns = [];
                $i = 0;
                while ($column = $statement->getColumnMeta($i++)) {
                    $columns[] = $this->getColumnMapping($column);
                }

                return $this->mapColumn($columns, $statement->fetchAll());
            } finally {
                if ($this->useCustomMapping) {
                    // reset custom mapping. when our first select is with custom mapping then we could have additional
                    // queries within one model->get() call as we could be eager loading relations. For eager loading we
                    // do not want custom mapping to be enabled
                    $this->resetCustomMapping();
                }
            }
        });
    }

    // TODO refactor to separate columnMapper class to ease testing
    private function mapColumn(array $columns, $resultSet)
    {
        $result = [];
        $count = count(current($resultSet));
        foreach ($resultSet as $item) {
            $row = [];
            for ($i = 0; $i < $count; $i++) {
                $currentColumnMeta = $columns[$i];
                $currentTable = $currentColumnMeta['table'];
                $columnName = $currentColumnMeta['name'];

                /**
                 * map column to root model or sub model. Currently we can only nest 2 level. So if something along
                 * query deals and join persons as one-to-one and persons one-to-one relations something more dynamtic must be implemented
                 * ala prefix persons one-to-one joined table aliases with "$person$..." and check table name on mapping
                 *
                 * This feature is currently not needed and not implemented
                 */
                if ($this->rootModel === $currentTable) {
                    $row[$columnName] = $item[$i];
                } else {
                    $row[$currentTable][$columnName] = $item[$i];
                }
            }
            $result[] = $row; // TODO maybe we could yield values here
        }
        return $result;
    }

    /**
     * @param string $rootModel
     * @return CustomMySqlConnection
     */
    public function useCustomMapping(string $rootModel): CustomMySqlConnection
    {
        $this->useCustomMapping = true;
        $this->rootModel = $rootModel;
        return $this;
    }

    private function resetCustomMapping()
    {
        $this->useCustomMapping = false;
        $this->rootModel = null;
        // reset back to PDO::FETCH_OBJ not to break ordinary queries that do not need custom mapping
        $this->fetchMode = PDO::FETCH_OBJ;
    }

    private function getColumnMapping(array $column)
    {
        $columnName = $column['name'];
        $currentTable = $column['table'];

        if ($columnName[0] === VirtualColumn::VIRTUAL_COLUMN_MARKER) {
            $currentTable = $this->rootModel;
            if (($endOfTablePart = strpos($columnName, VirtualColumn::VIRTUAL_COLUMN_MARKER, 1)) !== false) {
                $currentTable = substr($columnName, 1, $endOfTablePart - 1);
                $columnName = substr($columnName, $endOfTablePart + 1);
            }
        }

        return [
            'table' => $currentTable,
            'name' => $columnName
        ];
    }

}