<?php

namespace App\Database;


use App\Database\Column\Column;
use App\Database\Relations\Relation;

class QueryParts
{
    /**
     * @var Column[]
     */
    private $columns;

    /**
     * @var EagerLoad[]
     */
    private $eagerLoads;

    /**
     * @var Relation[]
     */
    private $relationJoins;

    public function __construct(array $columns, array $relationJoins, array $eagerLoads)
    {
        $this->columns = $columns ?: [];
        $this->relationJoins = $relationJoins ?: [];
        $this->eagerLoads = $eagerLoads ?: [];
    }

    /**
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return EagerLoad[]
     */
    public function getEagerLoads(): array
    {
        return $this->eagerLoads;
    }

    /**
     * @return Relation[]
     */
    public function getRelationJoins(): array
    {
        return $this->relationJoins;
    }
}