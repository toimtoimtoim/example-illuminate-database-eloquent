<?php

namespace App\Database;

trait CustomModelMappingTrait
{
    /**
     * instruct connection instance to use custom model mapping for the next select query
     *
     * @return $this
     */
    public function useCustomMappingForNextQuery()
    {
        $this->getConnection()->useCustomMapping($this->table);
        return $this;
    }
}