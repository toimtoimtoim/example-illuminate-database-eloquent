<?php

namespace App\Database\Column;

use App\Database\QueryContext;

/**
 * Is meant for cases when you would need to select something that does not exists as table column
 * ala "current_timestamp() as my_custom_column"
 */
class RawVirtualColumn extends VirtualColumn
{
    /**
     * @var string
     */
    private $sql;

    public function __construct($name, $sql)
    {
        parent::__construct($name);
        $this->sql = $sql;
    }

    public function applyColumnToQuery(QueryContext $queryContext, array $bindings = [])
    {
        $queryContext->getQuery()->selectRaw($this->sql . ' AS ' . $this->getName(), $bindings);
    }

    public function newWithPrefix($namePrefix)
    {
        return new static($namePrefix . $this->getName(), $this->sql);
    }
}