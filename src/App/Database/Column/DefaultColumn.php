<?php

namespace App\Database\Column;

use App\Database\QueryContext;

/**
 * Is ordinary column used in select clause
 */
class DefaultColumn implements Column
{
    /**
     * @var string
     */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function applyColumnToQuery(QueryContext $queryContext)
    {
        $queryContext->getQuery()->addSelect($this->name);
    }
}