<?php

namespace App\Database\Column;


use App\Database\QueryContext;

interface Column
{
    public function getName(): string;

    public function applyColumnToQuery(QueryContext $queryContext);
}