<?php

namespace App\Database\Column;


use App\Database\QueryContext;
use Illuminate\Support\Collection;

final class SelectColumnUtils
{
    private function __construct()
    {
        // util class
    }

    public static function extractColumnsForSelect($entityName, array $fields, array $virtualColumns)
    {
        $selectedFields = [];
        if (empty($virtualColumns)) {
            foreach ($fields as $selectedField) {
                $selectedFields[$selectedField] = new DefaultColumn($selectedField);
            }
            return $selectedFields;
        }

        $virtualColumnMap = (new Collection($virtualColumns))->mapWithKeys(function (VirtualColumn $virtualColumn) use ($entityName) {
            $virtualNamePrefix = VirtualColumn::VIRTUAL_COLUMN_MARKER . $entityName . VirtualColumn::VIRTUAL_COLUMN_MARKER;
            return [$entityName . '.' . $virtualColumn->getName() => $virtualColumn->newWithPrefix($virtualNamePrefix)];
        });

        foreach ($fields as $selectedField) {
            $virtualFieldSql = $virtualColumnMap->get($selectedField);
            $selectedFields[$selectedField] = $virtualFieldSql ?? new DefaultColumn($selectedField);
        }
        return $selectedFields;
    }

    /**
     * @param $selectColumns Column[]
     * @param QueryContext $queryContext
     */
    public static function applyColumnsToQuery($selectColumns, QueryContext $queryContext)
    {
        foreach ($selectColumns as $column) {
            $column->applyColumnToQuery($queryContext);
        }
    }

}