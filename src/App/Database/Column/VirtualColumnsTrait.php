<?php

namespace App\Database\Column;


trait VirtualColumnsTrait
{
    /**
     * Virtual columns
     *
     * Create this static property in your class if that model uses virtual columns
     * Trait based static variables can not be used as they are SHARED across all traited classes
     */
    //protected static $virtualColumns = [];

    /**
     * @return Column[]
     */
    public static function getVirtualColumns(): array
    {
        if (empty(static::$virtualColumns)) {
            return [];
        }
        return static::$virtualColumns;
    }

    /**
     * @param Column[] $virtualColumns
     */
    public static function setVirtualColumns(array $virtualColumns)
    {
        static::$virtualColumns = $virtualColumns;
    }
}