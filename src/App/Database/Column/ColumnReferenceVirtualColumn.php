<?php

namespace App\Database\Column;

use App\Database\QueryContext;
use App\Database\Relations\Relation;

/**
 * Is meant for cases when you would like to map some other table/relation column to current model field
 * ala map "persons.name" to "deals.person_name" virtual column
 */
class ColumnReferenceVirtualColumn extends VirtualColumn
{
    /**
     * @var Relation
     */
    private $relation;

    /**
     * @var string
     */
    private $columnOnRelation;

    public function __construct($name, Relation $relation, $columnOnRelation)
    {
        parent::__construct($name);
        $this->relation = $relation;
        $this->columnOnRelation = $columnOnRelation;
    }

    public function applyColumnToQuery(QueryContext $queryContext)
    {
        $query = $queryContext->getQuery();
        $query->addSelect($this->relation->getFieldName() . '.' . $this->columnOnRelation . ' AS ' . $this->getName());
        $this->relation->applyJoin($query, $queryContext->getUser());
    }

    public function newWithPrefix($namePrefix)
    {
        return new static($namePrefix . $this->getName(), $this->relation, $this->columnOnRelation);
    }
}