<?php

namespace App\Database\Column;

/**
 * Is data that you would like SQL to output as a column but this data does not exists on that table as a real column
 */
abstract class VirtualColumn implements Column
{
    /**
     * Virtual column is some synthetic column that we construct in sql and want to map to some model in hydration
     * To instruct hydration logic to detect virtual columns we need to prefix column alias with VIRTUAL_COLUMN_MARKER value
     *
     * "$query->selectRaw('current_timestamp() as $person$now_time');"
     *
     * will map column to "person" (sub) relation
     */
    const VIRTUAL_COLUMN_MARKER = '$';

    /**
     * @var string
     */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public abstract function newWithPrefix($namePrefix);
}