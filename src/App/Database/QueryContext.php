<?php

namespace App\Database;


use App\Domain\User\UserModel;

/**
 * DTO for current query related objects. Necessary as different parts of query building need to have access to
 * same objects within one service call scope. Helps to avoid method argument number crowing to big
 */
class QueryContext
{
    private $query;

    /**
     * @var UserModel|null
     */
    private $user;

    public function __construct($query, ?UserModel $user)
    {
        $this->query = $query;
        $this->user = $user;
    }

    /**
     * @return UserModel|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

}