<?php

namespace App\Database;


use App\Database\Relations\Relation;
use App\Database\Relations\RelationInstance;
use App\Field\FieldUtils;
use Illuminate\Support\Collection;

final class ModelUtils
{
    private function __construct()
    {
        // util class
    }

    public static function findRelationByFieldName(BaseModel $model, $fieldName): ?RelationInstance
    {
        $relations = new Collection($model::getAllRelations());

        $relation = $relations->first(function (Relation $relation) use ($fieldName) {
            return $relation->getFieldName() === $fieldName;
        });
        return $relation ? RelationInstance::fromRelation($relation) : null;
    }

    public static function findRelationByFieldNameRecursive(BaseModel $model, $fieldName): ?RelationInstance
    {
        [$firstFieldName, $trailingNames] = FieldUtils::getFieldPartForFirstDot($fieldName);
        return static::findRelationByFieldNameInternal($model, $firstFieldName, $trailingNames);
    }

    private static function findRelationByFieldNameInternal(BaseModel $model, $firstFieldName, $trailingNames = null, Relation $parentRelation = null): ?RelationInstance
    {
        $result = static::findRelationInstance($model, $firstFieldName, $parentRelation);
        if ($result) {
            if ($trailingNames) {
                [$firstFieldName, $trailingNames] = FieldUtils::getFieldPartForFirstDot($trailingNames);
                return static::findRelationByFieldNameInternal($result->getModelInstance(), $firstFieldName, $trailingNames, $result);
            }
            return $result;
        }

        foreach ($model::getAllRelations() as $relation) {
            $result = static::findRelationByFieldNameInternal($relation->getModelInstance(), $firstFieldName, $trailingNames, $result);
            if ($result) {
                if ($trailingNames) {
                    [$firstFieldName, $trailingNames] = FieldUtils::getFieldPartForFirstDot($trailingNames);
                    return static::findRelationByFieldNameInternal($result->getModelInstance(), $firstFieldName, $trailingNames, $result);
                }
                return $result;
            }
        }
        return $result;
    }

    private static function findRelationInstance(BaseModel $model, $fieldName, Relation $parentRelation = null): ?RelationInstance
    {
        $relations = new Collection($model::getAllRelations());

        $relation = $relations->first(function (Relation $relation) use ($fieldName) {
            return $relation->getFieldName() === $fieldName;
        });
        return $relation ? RelationInstance::fromRelation($relation, $parentRelation) : null;
    }

}