<?php

namespace App\Database\Relations;


use App\Database\BaseModel;
use LogicException;

trait RelationPropertiesTrait
{
    /**
     * @var string table name of entity to which relation belongs
     */
    protected $entity;

    /**
     * @var string field name in entity to this joined table is mapped
     */
    protected $fieldName;

    /**
     * @var string column name on entity which contains key used in join
     */
    protected $foreignKey;

    /**
     * @var string column name on mapped table which contains primary key used in join
     */
    protected $localKey;

    /**
     * @var string full class name of model
     */
    protected $relatedModelClass;

    /**
     * @var bool is relation extractable (use for one-to-many relations)
     */
    protected $extractable = true;

    /**
     * @var Permission
     */
    protected $permission;

    /** @var  bool */
    protected $isComplex = false;

    public static function getBuilder($entity, $fieldName = null, $related, $foreignKey = null, $localKey = null)
    {
        $builder = new RelationBuilder();
        $builder->setFieldName($fieldName);
        $builder->setEntity($entity);
        $builder->setLocalKey($localKey);
        $builder->setForeignKey($foreignKey);
        $builder->setModelClass($related);

        return $builder;
    }

    public function toBuilder(): RelationBuilder
    {
        $builder = new RelationBuilder();
        $builder->setEntity($this->entity);
        $builder->setFieldName($this->fieldName);
        $builder->setForeignKey($this->foreignKey);
        $builder->setLocalKey($this->localKey);
        $builder->setModelClass($this->relatedModelClass);
        $builder->setExtractable($this->extractable);
        $builder->setPermission($this->permission);
        $builder->setIsComplex($this->isComplex);

        return $builder;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return string
     */
    public function getQualifiedLocalKeyName(): string
    {
        return $this->relatedModelClass::TABLE . '.' . $this->localKey;
    }

    /**
     * @return string
     */
    public function getFieldNameWithTable(): string
    {
        return $this->entity . '.' . $this->fieldName;
    }

    public function getFieldTableAlias(): string
    {
        return $this->relatedModelClass::TABLE . ' AS ' . $this->fieldName;
    }

    /**
     * @return string
     */
    public function getForeignKey(): string
    {
        return $this->foreignKey;
    }

    /**
     * @return string
     */
    public function getLocalKey(): string
    {
        return $this->localKey;
    }

    /**
     * @return BaseModel
     * @throws LogicException
     */
    public function getModelInstance()
    {
        if (!$this->relatedModelClass) {
            throw new LogicException('modelClass not set!');
        }
        return new $this->relatedModelClass;
    }

    /**
     * @return mixed
     */
    public function getRelatedModelClass()
    {
        return $this->relatedModelClass;
    }

    /**
     * @return bool
     */
    public function isExtractable(): bool
    {
        return $this->extractable;
    }

    /**
     * @return Permission
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @return bool
     */
    public function isComplex(): bool
    {
        return $this->isComplex;
    }
}