<?php

namespace App\Database\Relations;

use App\Database\BaseModel;
use App\Database\QueryContext;

/**
 * Permission applied as WHERE in sql. having permissions in WHERE filters out rows from result set that should not be seen.
 * this is different from permission on join as most of join are LEFT joins - which do not filter rows out
 */
class DefaultPermissionOnWhere implements Permission
{
    /**
     * @var BaseModel
     */
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function apply(QueryContext $queryContext): void
    {
        $user = $queryContext->getUser();
        if (!$user || $user->isAdmin()) {
            return;
        }
        $this->applyToQuery($queryContext->getQuery(), $user);
    }

    protected function applyToQuery($query, $user): void
    {
        $ownerColumn = $this->model::TABLE . '.' . $this->model::OWNER_COLUMN;
        $query->where($ownerColumn, '=', $user->id);
    }
}