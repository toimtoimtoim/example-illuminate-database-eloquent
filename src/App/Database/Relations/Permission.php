<?php

namespace App\Database\Relations;

use App\Database\QueryContext;

interface Permission
{
    public function apply(QueryContext $queryContext): void;
}