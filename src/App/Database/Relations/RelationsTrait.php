<?php

namespace App\Database\Relations;

trait RelationsTrait
{
    /**
     * One to One relations
     *
     * Create this static property in your class if that model uses one-to-one relations
     * Trait based static variables can not be used as they are SHARED across all traited classes
     */
    //protected static $oneToOneRelations = [];

    /**
     * One to Many relations
     *
     * Create this static property in your class if that model uses one-to-many relations
     * Trait based static variables can not be used as they are SHARED across all traited classes
     */
    //protected static $oneToManyRelations = [];

    /**
     * @return Relation[]
     */
    public static function getOneToOneRelations(): array
    {
        if (empty(static::$oneToOneRelations)) {
            return [];
        }
        return static::$oneToOneRelations;
    }

    /**
     * @param Relation[] $oneToOneRelations
     */
    public static function setOneToOneRelations(array $oneToOneRelations)
    {
        static::$oneToOneRelations = $oneToOneRelations;
    }

    /**
     * @return Relation[]
     */
    public static function getOneToManyRelations(): array
    {
        if (empty(static::$oneToManyRelations)) {
            return [];
        }
        return static::$oneToManyRelations;
    }

    /**
     * @param Relation[] $oneToManyRelations
     */
    public static function setOneToManyRelations(array $oneToManyRelations)
    {
        static::$oneToManyRelations = $oneToManyRelations;
    }

    /**
     * @return Relation[]
     */
    public static function getAllRelations(): array
    {
        return array_merge(static::getOneToManyRelations(), static::getOneToOneRelations());
    }
}