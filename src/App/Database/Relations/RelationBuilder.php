<?php

namespace App\Database\Relations;


class RelationBuilder
{
    use RelationPropertiesTrait;

    public function __construct()
    {
        $this->permission = false;
    }

    /**
     * Return built instance of Relation
     *
     * @return Relation built instance
     */
    public function build()
    {
        return new Relation($this);
    }

    /**
     * @param string $fieldName
     * @return $this
     */
    public function setFieldName(string $fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @param string $modelClass
     * @return $this
     */
    public function setModelClass($modelClass)
    {
        $this->relatedModelClass = $modelClass;
        return $this;
    }

    /**
     * @param bool $extractable
     * @return $this
     */
    public function setExtractable(bool $extractable)
    {
        $this->extractable = $extractable;
        return $this;
    }

    /**
     * @param Permission $permission
     * @return $this
     */
    public function setPermission(?Permission $permission)
    {
        $this->permission = $permission;
        return $this;
    }

    /**
     * @param string $foreignKey
     * @return $this
     */
    public function setForeignKey(string $foreignKey)
    {
        $this->foreignKey = $foreignKey;
        return $this;
    }

    /**
     * @param string $entity
     * @return $this
     */
    public function setEntity(string $entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @param string $localKey
     * @return $this
     */
    public function setLocalKey(string $localKey)
    {
        $this->localKey = $localKey;
        return $this;
    }

    /**
     * @param bool $isComplex
     * @return $this
     */
    public function setIsComplex(bool $isComplex)
    {
        $this->isComplex = $isComplex;
        return $this;
    }
}