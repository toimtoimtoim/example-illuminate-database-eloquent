<?php

namespace App\Database\Relations;

use App\Database\QueryContext;

/**
 * Permission applied as "JOIN X ON (...permissions...)" part of in sql. Permission on join is useful for relation related permissions
 */
class DefaultPermissionOnJoin implements Permission
{
    /**
     * @var Relation
     */
    private $relation;

    public function __construct(Relation $relation)
    {
        $this->relation = $relation;
    }

    public function apply(QueryContext $queryContext): void
    {
        $user = $queryContext->getUser();
        if (!$user || $user->isAdmin()) {
            return;
        }

        $ownerColumn = $this->relation->getFieldName() . '.' . $this->relation->getRelatedModelClass()::OWNER_COLUMN;
        $queryContext->getQuery()->where($ownerColumn, '=', $user->id);
    }
}