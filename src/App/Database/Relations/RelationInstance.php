<?php

namespace App\Database\Relations;

/**
 * RelationInstance is concrete instance of Relation. When relation only describes how two models are related then
 * Relation instance describes how this object is related to main entity used in query.
 *
 * For example: this RelationInstance object is "pictures" relation and query was to fetch deals. So we could have
 * path to "pictures" as "deal->person->pictures".
 */
class RelationInstance extends Relation
{
    use RelationPropertiesTrait;

    /**
     * @var RelationInstance
     */
    private $parentRelation;

    public function __construct(RelationBuilder $builder, RelationInstance $parentRelation = null)
    {
        parent::__construct($builder);
        $this->parentRelation = $parentRelation;
    }

    public static function fromRelation(Relation $relation, RelationInstance $parentRelation = null)
    {
        return new static($relation->toBuilder(), $parentRelation);

    }

    /**
     * @return RelationInstance
     */
    public function getParentRelation(): ?RelationInstance
    {
        return $this->parentRelation;
    }

    /**
     * Get relation hierarchy as array and ordered so that root relation is always first and this relation last.
     * This kind of order is necessary for joins or we would get sql errors relation join is before its parent join in query
     *
     * @return array
     */
    public function getHierarchy(): array
    {
        $result = [];
        if ($this->parentRelation) {
            $result = $this->parentRelation->getHierarchy();
        }
        $result[] = $this;
        return $result;
    }

    public function getEntity(): string
    {
        // use parent relation field name as parent relation is aliased in sql and therefore we can not use entity which
        // contains table name
        $parent = $this->parentRelation;
        return $parent ? $parent->getFieldName() : $this->entity;
    }

}