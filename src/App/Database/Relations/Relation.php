<?php

namespace App\Database\Relations;

use App\Database\QueryContext;
use App\Database\QueryUtils;
use App\Domain\User\UserModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

/**
 * Relation abstraction that describes how one model (relatedModel) is related to its parent model (entity).
 * This description is only one way navigable from entity to relatedModel.
 *
 * If you need to navigate both ways create and use RelationInstance instances.
 */
class Relation
{
    use RelationPropertiesTrait;

    public function __construct(RelationBuilder $builder)
    {
        $this->fieldName = $builder->getFieldName();
        $this->entity = $builder->getEntity();
        $this->localKey = $builder->getLocalKey();
        $this->foreignKey = $builder->getForeignKey();
        $this->relatedModelClass = $builder->getRelatedModelClass();
        $this->extractable = $builder->isExtractable();
        $this->isComplex = $builder->isComplex();

        $permission = $builder->getPermission();
        $this->permission = $permission === false ? new DefaultPermissionOnJoin($this) : $permission;
    }

    /**
     * @return string
     */
    public function getSelectColumns(): string
    {
        return $this->fieldName . '.*';
    }

    public function applyJoin(Builder $query, ?UserModel $user)
    {
        $relationForeignKey = $this->getEntity() . '.' . $this->localKey;
        $tableName = $this->relatedModelClass::TABLE . ' as ' . $this->fieldName; // important! to have correct table names in column hydration part

        if ($this->isExtractable() && !QueryUtils::isColumnSelected($query, $relationForeignKey)) {
            $query->addSelect($relationForeignKey);
        }

        if (QueryUtils::isTableJoined($query, $tableName)) {
            return;
        }

        $query->leftJoin($tableName, function (JoinClause $join) use ($relationForeignKey, $user) {
            $relationPrimaryKeyField = $this->fieldName . '.' . $this->foreignKey;

            $join->on($relationForeignKey, '=', $relationPrimaryKeyField);

            $permission = $this->getPermission();
            if ($permission) {
                $permission->apply(new QueryContext($join, $user));
            }
        });
    }
}