<?php

namespace App\Database;

use App\Database\Column\VirtualColumnsTrait;
use App\Database\Relations\RelationsTrait;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use CustomModelMappingTrait;
    use RelationsTrait;
    use VirtualColumnsTrait;

    const OWNER_COLUMN = 'owner_id';

    /**
     * Create a new model instance that is existing.
     *
     * NB: this method is invoked in resultset hydration and we override it to provide our metadata related functionality
     *
     * @param array $attributes
     * @param null $connection
     * @return Model
     */
    public function newFromBuilder($attributes = [], $connection = null): Model
    {
        if (!is_array($attributes)) {
            // ordinary Eloquent hydration results stdClass instances instead of arrays. so we can call overridden method immediately
            return parent::newFromBuilder($attributes, $connection);
        }

        $relations = [];
        foreach (static::getOneToOneRelations() as $relation) {
            $fieldName = $relation->getFieldName();
            if (isset($attributes[$fieldName])) {
                // when primary key has value then (left) join was successful and we should move hydrated data to model
                // relations as new model instance.
                // after that it is ok to unset value as we do not need it within attributes
                if (!empty($attributes[$fieldName][$relation->getForeignKey()])) {
                    $relationInstance = $relation->getModelInstance()->newInstance([]);
                    $relationInstance->setRawAttributes($attributes[$fieldName]);
                    $relations[$fieldName] = $relationInstance;
                }
                unset($attributes[$fieldName]);
            }
        }
        $instance = parent::newFromBuilder($attributes, $connection);

        foreach ($relations as $name => $relation) {
            $instance->setRelation($name, $relation);
        }

        return $instance;
    }

    public function unsetRelation($relation)
    {
        unset($this->relations[$relation]);

        return $this;
    }

    public function setAttributeRaw($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }
}