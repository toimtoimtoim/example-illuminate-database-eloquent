<?php

namespace App;

use App\Database\BaseModel;
use App\Domain\User\UserService;
use App\Field\FieldValidatorService;
use App\Field\Partial\FieldParser;
use App\Filter\FilterNodesService;
use Illuminate\Support\Collection;
use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class AbstractEntityListController
{
    /**
     * @var FilterNodesService
     */
    private $filterService;

    /**
     * @var FieldValidatorService
     */
    private $fieldValidatorService;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(FilterNodesService $filterService, FieldValidatorService $fieldValidatorService, UserService $userService)
    {
        $this->filterService = $filterService;
        $this->fieldValidatorService = $fieldValidatorService;
        $this->userService = $userService;
    }

    protected abstract function getEntityService(): AbstractEntityListService;

    protected abstract function getEntityModelInstance(): BaseModel;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return mixed
     * @throws \RuntimeException
     */
    public function index(Request $request, Response $response, array $args)
    {
        $listArgs = new Collection();
        $unsafeArgs = new Collection($request->getQueryParams());
        $modelInstance = $this->getEntityModelInstance();

        if ($unsafeArgs->has('fields')) {
            $this->validateAndAddFields($unsafeArgs->get('fields'), $listArgs, $modelInstance);
        }

        if ($unsafeArgs->has('filter_id')) {
            $listArgs->put('filterId', (int)$unsafeArgs->get('filter_id'));
        }

        if ($unsafeArgs->has('filter')) {
            $filterString = trim($unsafeArgs->get('filter'), '"');
            $filter = $this->filterService->parseToNode($filterString);
            // TODO extract fields and validate field names
            $listArgs->put('filter', $filter);
        }

        // TODO transform to map here, not in service
        $listArgs->put('sort', $unsafeArgs->get('sort', $modelInstance->getTable() . '.id desc'));


        if ($unsafeArgs->has('offset')) {
            $listArgs->put('offset', (int)$unsafeArgs->get('offset'));
        }

        $limit = 100;
        if ($unsafeArgs->has('limit')) {
            $limit = (int)$unsafeArgs->get('limit');
        }
        $listArgs->put('limit', $limit + 1);

        $listArgs = $this->beforeIndexServiceCall($listArgs);

        $result = $this->getEntityService()->fetchNested($listArgs, $this->userService->getAuthenticatedUser());

        $result = $this->beforeIndexResponse($result);

        $hasMoreData = false;
        if ($limit && count($result) > $limit) {
            $hasMoreData = true;
            $result->pop();
        }

        $relatedObjects = RelatedObjectsExtractor::extract($result, true);

        return $response->withJson([
            'additional_data' => [
                'pagination' => [
                    'limit' => $limit,
                    'more_items_in_collection' => $hasMoreData,
                    'next_start' => $hasMoreData ? $listArgs->get('limit') : null,
                    'start' => $listArgs->get('offset', 0)
                ]
            ],
            'data' => $result,
            'related_objects' => $relatedObjects,
            'success' => true
        ]);
    }

    /**
     * Override to modify index method service call parameters before call is made
     *
     * Template method pattern: https://en.wikipedia.org/wiki/Template_method_pattern
     *
     * @param $listArgs Collection
     * @return Collection
     */
    protected function beforeIndexServiceCall($listArgs)
    {
        return $listArgs;
    }

    /**
     * Override to modify controller index method response data before it will be returned
     *
     * Template method pattern: https://en.wikipedia.org/wiki/Template_method_pattern
     *
     * @param $result Collection
     * @return Collection
     */
    protected function beforeIndexResponse($result)
    {
        return $result;
    }

    /**
     * parse field from query parameter and validate them against models
     */
    protected function validateAndAddFields($fieldsQueryParameter, Collection $listArgs, BaseModel $modelInstance)
    {
        $fields = FieldParser::parse($fieldsQueryParameter, $modelInstance->getTable());
        if ($fields) {
            $errors = $this->fieldValidatorService->validate($fields, $modelInstance);
            if ($errors) {
                throw new AppValidationException($errors);
            }
            $listArgs->put('fields', $fields);
        }
    }

}