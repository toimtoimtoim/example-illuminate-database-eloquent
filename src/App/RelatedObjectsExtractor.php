<?php

namespace App;

use App\Database\BaseModel;
use Illuminate\Support\Collection;

class RelatedObjectsExtractor
{
    /**
     * @var Collection
     */
    private $collection;
    /**
     * @var bool
     */
    private $unsetRelationsInModel;

    /**
     * @var array
     */
    private $entityFields = [];

    /**
     * @var array
     */
    private $relatedObjects = [];

    protected function __construct(Collection $collection, $unsetRelationsInModel = false)
    {
        $this->collection = $collection;
        $this->unsetRelationsInModel = $unsetRelationsInModel;
    }

    /**
     * Extract related relation object from all models in collection. IF $unsetRelationsInCollection is set to true then
     * extracted objects are set to null on model.
     *
     * @param Collection $collection collection of model from related relations are extracted
     * @param bool $unsetRelationsInModel should extracted relation object field be set to null
     * @return Collection collection of extracted relations grouped by relation field name on model
     */
    public static function extract(Collection $collection, $unsetRelationsInModel = false)
    {
        return (new static($collection, $unsetRelationsInModel))->extractInternal();
    }

    public function extractInternal()
    {
        if ($this->collection->isEmpty()) {
            return new Collection();
        }

        $this->initRelationsInfo($this->collection->first());

        foreach ($this->collection as $item) {
            $this->extractRelatedObjectsFromItem($item);
        }

        return new Collection($this->relatedObjects);
    }

    private function initRelationsInfo(BaseModel $model)
    {
        foreach ($model::getOneToManyRelations() as $relation) {
            if (!$relation->isExtractable()) {
                continue;
            }
            $this->entityFields[$relation->getFieldName()] = $relation->getModelInstance()->getKeyName();
            $this->relatedObjects[$relation->getFieldName()] = new Collection();
        }

        foreach ($model::getOneToOneRelations() as $relation) {
            if (!$relation->isExtractable()) {
                continue;
            }
            $this->entityFields[$relation->getFieldName()] = $relation->getForeignKey();
            $this->relatedObjects[$relation->getFieldName()] = new Collection();
        }
    }

    private function extractRelatedObjectsFromItem($item)
    {
        $itemRelations = $item->getRelations();
        foreach ($itemRelations as $entityField => $relationValue) {
            $foreignKey = $this->entityFields[$entityField] ?? null;
            if (!$foreignKey) {
                continue; // not extractable relation, lets move on
            }
            $this->extractRelationValue($item, $entityField, $foreignKey, $relationValue);
        }
    }

    private function extractRelationValue($item, $entityField, $foreignKey, $relationValue): void
    {
        if (empty($this->relatedObjects[$entityField][$foreignKey])) {
            if ($relationValue instanceof Collection) {
                $this->extractCollection($relationValue, $foreignKey, $entityField, $item);
            } else {
                $this->extractObject($relationValue, $foreignKey, $entityField, $item);
            }
        }
    }

    private function extractCollection($relationValue, $foreignKey, $entityField, $item): void
    {
        $ids = [];
        foreach ($relationValue as $entityItem) {
            $itemId = $entityItem[$foreignKey];
            $this->relatedObjects[$entityField]->put($itemId, $entityItem);
            $ids[] = $itemId;
        }
        if ($this->unsetRelationsInModel) {
            $item->unsetRelation($entityField);
            $item->setAttributeRaw($entityField, $ids);
        }
    }

    private function extractObject($relationValue, $foreignKey, $entityField, $item): void
    {
        $this->relatedObjects[$entityField]->put($relationValue[$foreignKey], $relationValue);
        if ($this->unsetRelationsInModel) {
            $item->unsetRelation($entityField);
        }
    }

}