<?php

namespace App;

final class Constants
{
    const ENV_DEV = 'DEV';
    const ENV_LIVE = 'LIVE';

    const DB_INSTANCE = 'db';

    const PARTIAL_RESPONSE_FIELDS_MAX_NESTING_LEVEL = 2;
}