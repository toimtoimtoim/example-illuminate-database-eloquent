<?php

namespace App;


/**
 * Class to represent validation errors
 */
class AppValidationException extends AppException
{
    /**
     * @var array
     */
    private $messages;

    public function __construct(array $messages)
    {
        $this->messages = $messages;

        parent::__construct($messages[0]);
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

}