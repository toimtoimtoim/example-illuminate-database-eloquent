<?php

namespace App;

use App\Database\BaseModel;
use App\Database\Column\SelectColumnUtils;
use App\Database\EagerLoad;
use App\Database\QueryContext;
use App\Database\QueryParts;
use App\Database\Relations\DefaultPermissionOnWhere;
use App\Domain\Filter\FilterService;
use App\Domain\User\UserModel;
use App\Field\FieldsToQueryPartsProcessor;
use App\Field\Partial\EntityFields;
use App\Filter\Node\ConditionsToNodesConverter;
use App\Filter\Node\FilterNodeUtils;
use App\Filter\Node\NodeToEloquentQueryBuilder;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection as SupportCollections;
use Illuminate\Support\Str;
use Monolog\Logger;

abstract class AbstractEntityListService
{
    /**
     * @var Manager
     */
    protected $db;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var FilterService
     */
    private $filterService;

    /**
     * @var BaseModel
     */
    private $modelInstance;

    /**
     * @var string
     */
    private $modelTable;

    /**
     * @var string
     */
    protected $modelClass;

    public function __construct(Manager $db, FilterService $filterService, Logger $logger)
    {
        $this->db = $db;
        $this->logger = $logger;
        $this->filterService = $filterService;

        $this->init();
    }

    protected abstract function getModelInstance();

    protected function init()
    {
        $this->modelInstance = $this->getModelInstance();
        $this->modelTable = $this->modelInstance->getTable();
        $this->modelClass = get_class($this->modelInstance);
    }

    /**
     * @param SupportCollections $args
     * @param UserModel|null $currentUser user instance in which 'privileges' query is made and permissions are set
     * @return EloquentCollection
     * @throws \App\AppException
     */
    public function fetchNested(SupportCollections $args = null, ?UserModel $currentUser): EloquentCollection
    {
        $args = $args ?? new SupportCollections();
        $queryContext = new QueryContext($this->modelInstance::query(), $currentUser);

        /**
         * Idea:
         *  extract these methods to strategy classes and use builder pattern so we could
         *  change strategies when creating service to get different behaviours
         */
        $queryParts = $this->extractQueryPartsFromFields($args);

        $this->applyColumnsToQuery($queryParts, $queryContext);
        $this->applyRelationJoinsToQuery($queryParts, $queryContext, $currentUser);
        $this->applyPermissionForRootModel($queryContext);
        $this->applyFilters($args, $queryContext);
        $this->applySorting($args, $queryContext);
        $this->applyPagination($args, $queryContext);

        $this->beforeFetchNestedSelectCall($queryContext);

        $this->modelInstance->useCustomMappingForNextQuery();
        $result = $queryContext->getQuery()->get();

        // eagerly load one-to-many relations to result objects if they were requested
        $eagerLoads = $queryParts->getEagerLoads();
        if (!empty($eagerLoads)) {
            $this->loadEagerLoadsToResult($eagerLoads, $result, $currentUser);
        }

        return $this->beforeFetchNestedResponse($result);
    }

    // TODO: extract to strategy class
    protected function applySorting(SupportCollections $args, QueryContext $queryContext)
    {
        if (!$args->has('sort')) {
            return;
        }

        // TODO missing custom sort aliases ala 'org_name asc' would be 'sort by organization.name asc'
        // TODO missing custom joins due field not being in partial fields list but needed for sort nevertheless

        $orderByField = explode(',', mb_strtolower($args->get('sort')));
        foreach ($orderByField as $item) {
            $column = trim($item);
            $order = 'asc';
            if (Str::endsWith($column, ' asc')) {
                $column = Str::replaceLast(' asc', '', $column);
            }
            if (Str::endsWith($column, ' desc')) {
                $column = Str::replaceLast(' desc', '', $column);
                $order = 'desc';
            }
            // FIXME needs validation. does (sub)entity has that field
            $queryContext->getQuery()->orderBy($column, $order);
        }
    }

    /**
     * Override to modify FetchNested query before select call is made
     *
     * Template method pattern: https://en.wikipedia.org/wiki/Template_method_pattern
     *
     * @param QueryContext $queryContext
     * @return QueryContext
     */
    protected function beforeFetchNestedSelectCall(QueryContext $queryContext)
    {
        return $queryContext;
    }

    /**
     * Override to modify FetchNested response data before it will be returned
     *
     * Template method pattern: https://en.wikipedia.org/wiki/Template_method_pattern
     *
     * @param $result EloquentCollection
     * @return EloquentCollection
     */
    protected function beforeFetchNestedResponse(EloquentCollection $result)
    {
        return $result;
    }

    // TODO: extract to strategy class
    protected function applyFilters(SupportCollections $args, QueryContext $queryContext): void
    {
        if ($args->has('filterId')) {
            // TODO validate that filter type is same as model. i.e. activity filter should not work with deal service
            $conditions = $this->filterService->getConditionsByFilterId($args->get('filterId'));
            $this->applyFilterNodes($queryContext, (new ConditionsToNodesConverter($conditions))->convert());
        } elseif ($args->has('filter')) {
            $this->applyFilterNodes($queryContext, $args->get('filter'));
        }
    }

    /**
     * Return QueryParts instance containing all field that are used for current service call. This will determine
     * which fields would be outputted - its is for partial fields or all entity fields
     *
     * TODO: extract to strategy class
     *
     * @param SupportCollections $args
     * @return QueryParts
     */
    protected function extractQueryPartsFromFields(SupportCollections $args): QueryParts
    {
        $queryPartsProcessor = new FieldsToQueryPartsProcessor();
        if ($args->has('fields')) {
            // we are doing partial request - for example return only 'id, name, org_id' fields
            return $queryPartsProcessor->process($this->modelInstance, $args->get('fields'));
        }
        // if no partial request is made (i.e. query does not have "fields" parameter) assume everything is wanted
        // and do "table.*" select with 2 levels nested relations fetching
        return $queryPartsProcessor->process($this->modelInstance, EntityFields::getForSelectAll($this->modelTable));
    }

    /**
     * Load one-to-many relations into root models or third level one-to-one/one-to-many relations into one-to-ones hydrated with root model
     *
     * TODO: extract to strategy class
     *
     * @param $eagerLoads EagerLoad[]
     * @param EloquentCollection $result
     * @param UserModel|null $user
     */
    protected function loadEagerLoadsToResult($eagerLoads, EloquentCollection $result, ?UserModel $user): void
    {
        foreach ($eagerLoads as $eagerLoad) {
            $parent = $eagerLoad->getFieldsParentPath();

            if ($parent === null) {
                // we are dealing with: eager loading one-to-many relations on root model
                $eagerLoad->loadToModels($result, $user);
            } else {
                if (false !== strpos($parent, '.')) {
                    throw new AppException('Eager loading is supported only up to 3 levels deep! Tried to load: ' . $parent);
                }

                // we are dealing with: eager loading one-to-one or one-to-many relations for one-to-one relations joined along with root model
                // extract these one-to-one relations from root model collection and do load eager for them.
                $parentModels = $result->map(function (BaseModel $obj) use ($parent) {
                    return $obj->getRelations()[$parent] ?? null;
                })->reject(function ($relation) {
                    return null === $relation;
                });

                if ($parentModels->isNotEmpty()) {
                    $eagerLoad->loadToModels($parentModels, $user);
                }

            }
        }
    }

    /**
     * Apply columns from query parts (extracted from fields) to query. These are fields that root model would contain
     * in response.
     *
     * TODO: extract to strategy class
     *
     * @param QueryParts $queryParts
     * @param QueryContext $queryContext
     */
    protected function applyColumnsToQuery(QueryParts $queryParts, QueryContext $queryContext): void
    {
        SelectColumnUtils::applyColumnsToQuery($queryParts->getColumns(), $queryContext);
    }

    /**
     * Apply relations joins from query parts (extracted from fields) to query. Relation joins are derived from
     * columns that are outputted in response
     *
     * TODO: extract to strategy class
     *
     * @param QueryParts $queryParts
     * @param QueryContext $queryContext
     * @param UserModel|null $user user in which permission join is done
     */
    protected function applyRelationJoinsToQuery(QueryParts $queryParts, QueryContext $queryContext, ?UserModel $user): void
    {
        $query = $queryContext->getQuery();
        foreach ($queryParts->getRelationJoins() as $relation) {
            $relation->applyJoin($query, $user);
        }
    }

    /**
     * TODO: extract to strategy class
     *
     * @param SupportCollections $args
     * @param QueryContext $queryContext
     * @internal param $query
     */
    protected function applyPagination(SupportCollections $args, QueryContext $queryContext): void
    {
        $query = $queryContext->getQuery();

        if ($args->has('offset')) {
            $query->skip($args->get('offset'));
        }
        $query->limit($args->get('limit', 100));
    }

    /**
     * TODO: extract to strategy class
     *
     * @param $queryContext
     */
    protected function applyPermissionForRootModel(QueryContext $queryContext): void
    {
        $permission = $this->getPermission();

        // permission should be applied as where closure so all permission related conditions would be enclosed in parenthesis
        $queryContext->getQuery()->where(function ($query) use ($permission, $queryContext) {
            $subContext = new QueryContext($query, $queryContext->getUser());
            $permission->apply($subContext);
        });
    }

    protected function getPermission()
    {
        return new DefaultPermissionOnWhere($this->modelClass);
    }

    /**
     * @param QueryContext $queryContext
     * @param $rootFilterNode
     */
    protected function applyFilterNodes(QueryContext $queryContext, $rootFilterNode): void
    {
        $isComplex = FilterNodeUtils::isFilterComplex($rootFilterNode, $this->modelInstance);
        if ($isComplex) {
            NodeToEloquentQueryBuilder::applyToQueryAsSubQuery($this->modelInstance, $rootFilterNode, $queryContext->getQuery(), $queryContext->getUser());
        } else {
            NodeToEloquentQueryBuilder::applyToQuery($this->modelInstance, $rootFilterNode, $queryContext->getQuery(), null, $queryContext->getUser());
        }
    }
}