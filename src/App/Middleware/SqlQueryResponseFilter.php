<?php

namespace App\Middleware;

use App\Constants;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class SqlQueryResponseFilter implements FilterInterface
{
    /**
     * @var ContainerInterface
     */
    private $ci;

    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    /**
     * Middleware filter to log sql queries to logs
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param  callable $next Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {

        // enable sql query logging to see executed queries
        $this->ci->get(Constants::DB_INSTANCE)->getDatabaseManager()->connection()->enableQueryLog();

        // process request
        $response = $next($request, $response);

        $queries = $this->ci->get(Constants::DB_INSTANCE)->getConnection()->getQueryLog();
        $this->ci->get('logger')->notice('SQL: ' . json_encode($queries));

        return $response;
    }
}