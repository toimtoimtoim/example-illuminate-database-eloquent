<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface FilterInterface
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next);

}