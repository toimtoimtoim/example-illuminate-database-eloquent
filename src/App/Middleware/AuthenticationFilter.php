<?php

namespace App\Middleware;


use App\AppException;
use App\Domain\User\UserService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AuthenticationFilter implements FilterInterface
{
    const API_TOKEN_PARAM = 'API_TOKEN';

    /**
     * @var ContainerInterface
     */
    private $ci;

    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $this->authenticateWithApiToken($request->getQueryParam(static::API_TOKEN_PARAM));

        return $next($request, $response);
    }

    private function authenticateWithApiToken($apiToken): void
    {
        if ($apiToken) {
            $userService = $this->ci->get(UserService::class);
            try {
                $user = $userService->findUserByApiToken($apiToken);
                $userService->setAuthenticatedUser($user);
            } catch (ModelNotFoundException $e) {
                throw new AppException('Incorrect API_TOKEN provided'); // TODO securityException maybe?
            }
        } else {
            throw new AppException('API_TOKEN missing from request');
        }
    }
}