<?php

namespace App\Filter\Parser;


use Dissect\Lexer\SimpleLexer;

class SqlLexer extends SimpleLexer
{
    public function __construct()
    {
        $this->regex('COL', '/^[A-Za-z0-9_\.]+/');
        $this->token('(');
        $this->token(')');
        // binary expressions
        $this->token('&&');
        $this->token('||');
        // comparison nodes
        $this->token('<=');
        $this->token('>=');
        $this->token('<');
        $this->token('>');
        $this->token('=');
        $this->token('!=');

        $this->regex('WSP', "/^[ \r\n\t]+/");
        $this->skip('WSP');
    }
}