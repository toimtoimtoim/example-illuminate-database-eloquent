<?php

namespace App\Filter\Parser;

use App\Filter\Node\BinaryExpressionNode;
use App\Filter\Node\FieldNode;
use App\Filter\Node\FilterConditionNode;
use Dissect\Parser\Grammar;

class SqlBuilderGrammar extends Grammar
{
    public $fields = [];

    public function __construct()
    {
        $this('AND')
            ->is('AND', '&&', 'OR')
            ->call(function ($left, $op, $right) {
                return new BinaryExpressionNode($left, 'AND', $right);
            })
            ->is('OR');

        $this('OR')
            ->is('OR', '||', 'COMPARISON')
            ->call(function ($left, $op, $right) {
                return new BinaryExpressionNode($left, 'OR', $right);
            })
            ->is('COMPARISON');

        $fields = &$this->fields;
        $setField = function ($field, $value) use (&$fields) {
            $fields[] = [$field, $value];
        };

        $comparisonFunction = function ($left, $op, $right) use ($setField) {
            $setField($left, $right);

            $fieldString = $left->getValue();

            $lastDotPosition = strrpos($fieldString, '.');
            if ($lastDotPosition === false) {
                $field = $fieldString;
                $relation = '';
            } else {
                $field = substr($fieldString, $lastDotPosition + 1);
                $relation = substr($fieldString, 0,$lastDotPosition);
            }

            return new FilterConditionNode(
                $relation,
                $field,
                $op->getType(),
                $right->getValue(),
                null
            );
        };

        $this('COMPARISON')
            ->is('FIELD', '=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '>', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '<', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '>=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '<=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '!=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD');

        $this('FIELD')
            ->is('COL')
            ->call(function ($col) {
                return new FieldNode($col->getValue());
            })
            ->is('(', 'AND', ')')
            ->call(function ($left, $expression, $right) {
                return $expression;
            });

        $this->start('AND');
    }

}