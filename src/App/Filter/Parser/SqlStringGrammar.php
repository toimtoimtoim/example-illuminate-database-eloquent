<?php

namespace App\Filter\Parser;

use Dissect\Parser\Grammar;

class SqlStringGrammar extends Grammar
{
    public $fields = [];

    public function __construct()
    {
        $this('AND')
            ->is('AND', '&&', 'OR')
            ->call(function ($left, $op, $right) {
                return 'AND(' . $left . ',' . $right . ')';
            })
            ->is('OR');

        $this('OR')
            ->is('OR', '||', 'COMPARISON')
            ->call(function ($left, $op, $right) {
                return 'OR(' . $left . ',' . $right . ')';
            })
            ->is('COMPARISON');

        $fields = &$this->fields;
        $setField = function ($field, $value) use (&$fields) {
            $fields[] = [$field, $value];
        };

        $comparisonFunction = function ($left, $op, $right) use ($setField) {
            $setField($left, $right);
            return $op->getType() . '(' . $left . ',' . $right . ')';
        };

        $this('COMPARISON')
            ->is('FIELD', '=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '>', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '<', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '>=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '<=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD', '!=', 'FIELD')->call($comparisonFunction)
            ->is('FIELD');

        $this('FIELD')
            ->is('COL')
            ->call(function ($col) {
                return $col->getValue();
            })
            ->is('(', 'AND', ')')
            ->call(function ($left, $expression, $right) {
                return $expression;
            });

        $this->start('AND');
    }

}