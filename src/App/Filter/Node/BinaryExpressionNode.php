<?php

namespace App\Filter\Node;

use Dissect\Node\Node;

class BinaryExpressionNode extends CommonNode
{
    const NODE_COUNT = 2;

    const OP_AND = 'AND';
    const OP_OR = 'OR';
    const OPERATORS = [self::OP_AND, self::OP_OR];

    /**
     * @param Node $left
     * @param string $op
     * @param Node $right
     */
    public function __construct(Node $left, $op, Node $right)
    {
        parent::__construct(['operator' => $op], [
            'left' => $left,
            'right' => $right,
        ]);
    }

    public function getLeft()
    {
        return $this->getNode('left');
    }

    public function getRight()
    {
        return $this->getNode('right');
    }

    public function getOperator()
    {
        return $this->getAttribute('operator');
    }
}
