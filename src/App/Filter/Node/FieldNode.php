<?php

namespace App\Filter\Node;

class FieldNode extends CommonNode
{
    /**
     * @param string $value
     */
    public function __construct($value)
    {
        parent::__construct(['value' => $value]);
    }

    public function getValue()
    {
        return $this->getAttribute('value');
    }
}