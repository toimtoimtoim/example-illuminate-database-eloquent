<?php

namespace App\Filter\Node;


use App\AppException;
use App\Domain\Filter\FilterConditionModel;
use Dissect\Node\Node;
use Illuminate\Support\Collection;

class ConditionsToNodesConverter
{
    /**
     * @var Collection
     */
    private $conditions;
    /**
     * @var Collection
     */
    private $groupedByParentId;

    /**
     * @var Collection
     */
    private $nodes;

    /**
     * @var Node
     */
    private $rootNode;

    public function __construct(Collection $conditions)
    {
        $this->conditions = $conditions->keyBy('id');
        $this->groupedByParentId = $conditions->groupBy('parent_id');
        $this->nodes = new Collection([]);
    }

    /**
     * Convert conditions to (Abstract Syntax Tree) nodes
     *
     * @return Node root node of converted conditions
     *
     * @throws \App\AppException
     */
    public function convert(): Node
    {
        if ($this->rootNode === null) {
            foreach ($this->conditions as $condition) {
                $this->convertCondition($condition);
            }
        }

        if ($this->rootNode === null) {
            throw new AppException('RootNode not found for converted filter conditions!');
        }

        return $this->rootNode;
    }

    private function convertCondition(FilterConditionModel $condition)
    {
        $currentNode = null;
        if (in_array($condition->operator, BinaryExpressionNode::OPERATORS, true)) {
            $currentNode = $this->convertBinaryExpression($condition);
        } else {
            $currentNode = new FilterConditionNode(
                $condition->relation,
                $condition->field_id,
                $condition->operator,
                $condition->value,
                $condition->extra_value
            );
        }

        $this->nodes->put($condition->id, $currentNode);

        if ($condition->parent_id === null) {
            if ($this->rootNode !== null) {
                throw new AppException('Filter can not have more then one root node! FilterId: ' . $condition->id);
            } else {
                $this->rootNode = $currentNode;
            }

        }
        return $currentNode;
    }


    private function getNodeOrConvert(FilterConditionModel $condition)
    {
        $node = $this->nodes->get($condition->id);
        if ($node === null) {
            $node = $this->convertCondition($condition);
        }
        return $node;
    }

    private function convertBinaryExpression(FilterConditionModel $condition): BinaryExpressionNode
    {
        $childNodes = $this->groupedByParentId->get($condition->id);
        if (count($childNodes) !== BinaryExpressionNode::NODE_COUNT) {
            throw new AppException('BinaryExpression must have always 2 child nodes! FilterConditionId: ' . $condition->id);
        }

        $currentNode = new BinaryExpressionNode(
            $this->getNodeOrConvert($childNodes[0]),
            $condition->operator,
            $this->getNodeOrConvert($childNodes[1])
        );
        return $currentNode;
    }

}