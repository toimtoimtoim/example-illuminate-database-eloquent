<?php

namespace App\Filter\Node;


class FilterConditionNode extends CommonNode
{
    public function __construct($relation, $field, $operator, $value, $extraValue = null)
    {
        parent::__construct([
            'relation' => $relation,
            'field' => $field,
            'operator' => $operator,
            'value' => $value,
            'extraValue' => $extraValue
        ]);
    }

    public function getValue()
    {
        return $this->getAttribute('value');
    }

    public function getExtraValue()
    {
        return $this->getAttribute('extraValue');
    }

    public function getRelation()
    {
        return $this->getAttribute('relation');
    }

    public function getField()
    {
        return $this->getAttribute('field');
    }

    public function getOperator()
    {
        return $this->getAttribute('operator');
    }
}