<?php

namespace App\Filter\Node;


use App\Database\BaseModel;
use App\Database\ModelUtils;
use App\Filter\FilterException;
use Dissect\Node\Node;

final class FilterNodeUtils
{
    private function __construct()
    {
        // utils class
    }

    public static function isFilterComplex(Node $node, BaseModel $rootModel): bool
    {
        $result = false;
        if ($node instanceof BinaryExpressionNode) {
            $left = $node->getLeft();
            if ($left && static::isFilterComplex($left, $rootModel)) {
                return true;
            }

            $right = $node->getRight();
            if ($right && static::isFilterComplex($right, $rootModel)) {
                return true;
            }
        } elseif ($node instanceof FilterConditionNode) {
            $result = static::isFilterNodeComplex($node, $rootModel);
        } else {
            throw new FilterException('Unknown node type.');
        }

        return $result;
    }

    private static function isFilterNodeComplex(FilterConditionNode $node, BaseModel $rootModel): bool
    {
        $relation = ModelUtils::findRelationByFieldNameRecursive($rootModel, $node->getRelation());
        return $relation && $relation->isComplex();
    }

}