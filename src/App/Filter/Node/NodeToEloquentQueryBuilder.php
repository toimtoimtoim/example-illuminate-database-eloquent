<?php

namespace App\Filter\Node;


use App\AppException;
use App\Database\BaseModel;
use App\Database\ModelUtils;
use App\Database\QueryUtils;
use App\Database\Relations\RelationInstance;
use App\Domain\User\UserModel;
use App\Filter\FilterException;
use App\Field\FieldUtils;
use Dissect\Node\Node;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Expression;

/**
 * Apply node (nodes of abstract syntax tree) contents to Eloquent query - this builds where conditions for database query
 *
 * This class is responsible for:
 * 1) building WHERE conditions
 * 2) adding necessary JOIN clauses if filter condition relation has not already been included in query
 */
class NodeToEloquentQueryBuilder
{
    /**
     * @var Builder
     */
    private $query;
    /**
     * @var BaseModel
     */
    private $rootModel;

    /**
     * is used to add joins. This is limitation of eloquent query builder - when WHERE part contains conditions wraped
     * in parenthesis and these conditions need to add join these joins must be applied to root query
     *
     * @var Builder
     */
    private $rootQuery;

    /**
     * @var UserModel
     */
    private $user;

    public function __construct(BaseModel $rootModel, Builder $query, Builder $rootQuery = null, UserModel $user = null)
    {
        $this->rootModel = $rootModel;
        $this->query = $query;
        $this->rootQuery = $rootQuery ?? $query;
        $this->user = $user;
    }

    public function apply(Node $node)
    {
        // TODO should method be in node class and each type will implement each own way it?
        if ($node instanceof BinaryExpressionNode) {
            $left = $node->getLeft();
            if ($left instanceof FilterConditionNode) {
                $this->applyFilterCondition($left, 'AND');
            } elseif ($left instanceof BinaryExpressionNode) {
                $this->applyBinaryOperator($node, $left);
            }

            $right = $node->getRight();
            if ($right instanceof FilterConditionNode) {
                $this->applyFilterCondition($right, $node->getOperator());
            } elseif ($right instanceof BinaryExpressionNode) {
                $this->applyBinaryOperator($node, $right);
            } else {
                throw new FilterException('Unknown node type.');
            }
        } elseif ($node instanceof FilterConditionNode) {
            $this->applyFilterCondition($node, 'AND');
        } else {
            throw new FilterException('Unknown node type.');
        }
    }

    private function applyBinaryOperator(Node $node, $subNode): void
    {
        $rootModel = $this->rootModel;
        $rootQuery = $this->rootQuery ?? $this->query;

        $this->query->where(
            function ($subQuery) use ($rootQuery, $subNode, $rootModel) {
                static::applyToQuery($rootModel, $subNode, $subQuery, $rootQuery, $this->user);
            },
            null,
            null,
            $node->getOperator()
        );
    }

    protected function applyFilterCondition(FilterConditionNode $filterCondition, $whereOperator): void
    {
        $this->query->where(
            $this->getFullFieldPath($filterCondition),
            $filterCondition->getOperator(),
            $filterCondition->getValue(),
            $whereOperator
        );

        $relationFieldName = $filterCondition->getRelation();
        if ($relationFieldName) {
            $this->applyRelationJoin($relationFieldName);
        }
    }

    public static function applyToQuery(BaseModel $rootModel, Node $node, Builder $query, Builder $rootQuery = null, UserModel $user = null)
    {
        $nodeToBuilder = new NodeToEloquentQueryBuilder($rootModel, $query, $rootQuery, $user);
        $nodeToBuilder->apply($node);
    }

    /**
     * For complex filter (the ones that need to join one-to-many relations) apply filtering as subquery join so
     * we would not have problems when joins return multiple (duplicate) rows for same entity
     */
    public static function applyToQueryAsSubQuery(BaseModel $rootModel, Node $node, Builder $query, UserModel $user = null)
    {
        $complexFilterQuery = $rootModel::query();

        $complexTablePk = '_complexFilter.' . $rootModel->getKeyName();
        $rootTablePk = $rootModel->getQualifiedKeyName();

        // apply filter to subquery
        static::applyToQuery($rootModel, $node, $complexFilterQuery, null, $user);

        // only column we really need to select is root model primary key as we used it to join subquery to main query
        $complexFilterQuery->select([$rootTablePk]);

        // join subquery as raw sql to main query. currently this is only way to do subquery joins
        $sql = $complexFilterQuery->toSql();
        $query->join(
            new Expression("({$sql}) as _complexFilter"),
            $complexTablePk,
            '=',
            $rootTablePk
        )->mergeBindings($complexFilterQuery->getQuery());
    }

    /**
     * @param $fieldName
     */
    protected function applyRelationJoin($fieldName): void
    {
        $relationOnModel = ModelUtils::findRelationByFieldNameRecursive($this->rootModel, $fieldName);
        if (!$relationOnModel) {
            throw new AppException('Filter condition tries to join unknown relation for model: ' . $this->rootModel::TABLE . ', relation: ' . $fieldName);
        }

        $query = $this->rootQuery ?? $this->query;
        $this->joinRelation($query, $relationOnModel);
    }

    /**
     * @param FilterConditionNode $filterCondition
     * @return string
     */
    protected function getFullFieldPath(FilterConditionNode $filterCondition): string
    {
        $relationPath = $filterCondition->getRelation();
        if (!$relationPath) {
            // filter conditions on root model does not have relation specified in database but we need to have it in sql
            $relationPath = $this->rootModel::TABLE;
        }
        return FieldUtils::subStringAfterLastDot($relationPath) . '.' . $filterCondition->getField();
    }

    /**
     * @param $query
     * @param $relationOnModel
     */
    private function joinRelation($query, RelationInstance $relationOnModel): void
    {
        foreach ($relationOnModel->getHierarchy() as $relationInstance) {
            if (!QueryUtils::isTableJoined($query, $relationInstance->getFieldTableAlias())) {
                $relationInstance->applyJoin($query, $this->user);
            }
        }
    }
}