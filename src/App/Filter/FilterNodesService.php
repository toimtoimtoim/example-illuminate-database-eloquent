<?php

namespace App\Filter;


use Dissect\Lexer\AbstractLexer;
use Dissect\Parser\Grammar;
use Dissect\Parser\LALR1\Parser;

class FilterNodesService
{
    private $lexer;
    private $parser;
    private $grammar;

    public function __construct(AbstractLexer $lexer, Grammar $grammar)
    {
        $this->lexer = $lexer;
        $this->grammar = $grammar;
        $this->parser = new Parser($this->grammar);
    }

    public function parseToNode(string $filterString)
    {
        $stream = $this->lexer->lex(mb_strtolower($filterString));
        return $this->parser->parse($stream);
    }

}