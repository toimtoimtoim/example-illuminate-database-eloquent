<?php

namespace App\Domain\Picture;

use App\Database\BaseModel;

class PictureModel extends BaseModel
{
    const ITEM_TYPE_PERSON = 'person';
    const ITEM_TYPE_PRODUCT = 'product';
    const ITEM_TYPE_ORGANIZATION = 'organization';

    const TABLE = 'pictures';
    protected $table = self::TABLE;
}