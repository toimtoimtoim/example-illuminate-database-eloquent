<?php

namespace App\Domain\Organization;

use App\Database\BaseModel;
use App\Database\Relations\Relation;
use App\Domain\User\UserModel;

class OrganizationModel extends BaseModel
{
    const TABLE = 'org';
    protected $table = self::TABLE;

    protected static $oneToOneRelations = [];
    protected static $oneToManyRelations = [];

    protected static function bootRelationsTrait()
    {
        static::setOneToOneRelations([
            Relation::getBuilder(static::TABLE, 'owner', UserModel::class, 'id', 'user_id')->build()
        ]);
    }
}