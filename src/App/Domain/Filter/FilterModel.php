<?php

namespace App\Domain\Filter;

use App\Database\BaseModel;

class FilterModel extends BaseModel
{
    const TABLE = 'filters';
    protected $table = self::TABLE;

    public function conditions()
    {
        return $this->hasMany(FilterConditionModel::class, 'filter_id');
    }
}