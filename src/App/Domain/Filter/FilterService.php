<?php

namespace App\Domain\Filter;


class FilterService
{
    public function getById($id) {
        return FilterModel::findOrFail($id)->get();
    }

    public function getConditionsByFilterId($filterId) {
        return FilterConditionModel::where('filter_id', '=', $filterId)->get();
    }
}