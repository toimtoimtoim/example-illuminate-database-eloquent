<?php

namespace App\Domain\Filter;


use App\Database\BaseModel;

class FilterConditionModel extends BaseModel
{
    const TABLE = 'filter_conditions';
    protected $table = self::TABLE;

    protected $fillable = ['filter_id', 'parent_id', 'operator', 'relation', 'field_id', 'value', 'extra_value'];

    public function filter()
    {
        return $this->belongsTo(FilterModel::class, 'id', 'filter_id');
    }
}