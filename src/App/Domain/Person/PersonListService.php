<?php

namespace App\Domain\Person;


use App\AbstractEntityListService;

class PersonListService extends AbstractEntityListService
{
    protected function getModelInstance()
    {
        return new PersonModel();
    }
}