<?php

namespace App\Domain\Person;


use App\AbstractEntityListController;
use App\AbstractEntityListService;
use App\Database\BaseModel;
use App\Domain\User\UserService;
use App\Field\FieldValidatorService;
use App\Filter\FilterNodesService;

class PersonListController extends AbstractEntityListController
{
    private $personService;

    public function __construct(
        FilterNodesService $filterService,
        FieldValidatorService $fieldValidatorService,
        UserService $userService,
        PersonListService $personService
    )
    {
        parent::__construct($filterService, $fieldValidatorService, $userService);
        $this->personService = $personService;
    }

    protected function getEntityService(): AbstractEntityListService
    {
        return $this->personService;
    }

    protected function getEntityModelInstance(): BaseModel
    {
        return new PersonModel();
    }
}