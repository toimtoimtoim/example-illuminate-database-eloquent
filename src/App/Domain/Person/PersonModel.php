<?php

namespace App\Domain\Person;

use App\Database\BaseModel;
use App\Database\Relations\Relation;
use App\Domain\Activity\ActivityModel;
use App\Domain\Activity\Participant\ActivityParticipantModel;
use App\Domain\Organization\OrganizationModel;
use App\Domain\Picture\PictureModel;
use App\Domain\User\UserModel;

class PersonModel extends BaseModel
{
    const TABLE = 'persons';
    protected $table = self::TABLE;

    protected static $oneToOneRelations = [];
    protected static $oneToManyRelations = [];

    protected static function bootRelationsTrait()
    {
        static::setOneToOneRelations([
            Relation::getBuilder(static::TABLE, 'organization', OrganizationModel::class, 'id', 'org_id')->build(),
            Relation::getBuilder(static::TABLE, 'owner', UserModel::class, 'id', 'user_id')->build(),
        ]);

        static::setOneToManyRelations([
            Relation::getBuilder(static::TABLE, 'pictures', PictureModel::class, 'item_id', 'id')
                ->setExtractable(true)->setPermission(null)->setIsComplex(true)->build()
        ]);

    }

    public function pictures()
    {
        return $this->hasMany(PictureModel::class, 'item_id')->where('item_type', '=', PictureModel::ITEM_TYPE_PERSON);
    }

    public function activities()
    {
        return $this->belongsToMany(
            ActivityModel::class,
            ActivityParticipantModel::TABLE,
            'person_id',
            'activity_id'
        );
    }
}