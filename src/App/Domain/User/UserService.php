<?php

namespace App\Domain\User;

class UserService
{
    /**
     * @var UserModel
     */
    private $authenticatedUser;

    public function getAuthenticatedUser()
    {
        return $this->authenticatedUser;
    }

    /**
     * @param UserModel $authenticatedUser
     * @return UserService
     */
    public function setAuthenticatedUser(UserModel $authenticatedUser): UserService
    {
        $this->authenticatedUser = $authenticatedUser;
        return $this;
    }

    /**
     * @param $id int
     * @return UserModel
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findUserById($id)
    {
        return UserModel::findOrFail($id);
    }

    /**
     * @param $apiToken
     * @return UserModel
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findUserByApiToken($apiToken)
    {
        return UserModel::select(UserModel::TABLE . '.*')
            ->join(ApiTokenModel::TABLE, UserModel::TABLE . '.id', '=', ApiTokenModel::TABLE . '.user_id')
            ->where(ApiTokenModel::TABLE . '.token', '=', $apiToken)
            ->where(ApiTokenModel::TABLE . '.active_flag', '=', 1)
            ->firstOrFail();
    }
}