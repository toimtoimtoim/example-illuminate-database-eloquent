<?php

namespace App\Domain\User;


use App\Database\BaseModel;

class ApiTokenModel extends BaseModel
{
    const TABLE = 'api_token';
    protected $table = self::TABLE;

}