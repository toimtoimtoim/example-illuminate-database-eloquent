<?php

namespace App\Domain\User;

use App\Database\BaseModel;

class UserModel extends BaseModel
{
    const TABLE = 'users';
    protected $table = self::TABLE;

    public function apiTokens()
    {
        return $this->hasMany(ApiTokenModel::class, 'user_id');
    }

    public function isAdmin()
    {
        return $this->is_admin_flag === 1;
    }
}