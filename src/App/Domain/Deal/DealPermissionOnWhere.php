<?php

namespace App\Domain\Deal;


use App\Database\Relations\DefaultPermissionOnWhere;
use Illuminate\Database\Query\Expression;

class DealPermissionOnWhere extends DefaultPermissionOnWhere
{
    protected function applyToQuery($query, $user): void
    {
        parent::applyToQuery($query, $user);

        // or user is following deal
        $query->orWhereExists(function ($query) use ($user) {
            $query->select(new Expression('1'))
                ->from('deal_follow')
                ->where('deal_follow.user_id', '=', $user->id)
                ->whereRaw('deals.id = deal_follow.deal_id');
        });
    }

}