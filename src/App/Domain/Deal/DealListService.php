<?php

namespace App\Domain\Deal;

use App\AbstractEntityListService;

class DealListService extends AbstractEntityListService
{
    protected function getModelInstance()
    {
        return new DealModel();
    }

    protected function getPermission()
    {
        return new DealPermissionOnWhere($this->modelClass);
    }
}