<?php

namespace App\Domain\Deal;

use App\AbstractEntityListController;
use App\AbstractEntityListService;
use App\Database\BaseModel;
use App\Domain\User\UserService;
use App\Field\FieldValidatorService;
use App\Filter\FilterNodesService;

class DealListController extends AbstractEntityListController
{
    private $dealService;

    public function __construct(
        FilterNodesService $filterService,
        FieldValidatorService $fieldValidatorService,
        UserService $userService,
        DealListService $dealService
    )
    {
        parent::__construct($filterService, $fieldValidatorService, $userService);
        $this->dealService = $dealService;
    }

    protected function getEntityService(): AbstractEntityListService
    {
        return $this->dealService;
    }

    protected function getEntityModelInstance(): BaseModel
    {
        return new DealModel();
    }
}