<?php

namespace App\Domain\Deal;

use App\Database\BaseModel;
use App\Database\Column\ColumnReferenceVirtualColumn;
use App\Database\Column\RawVirtualColumn;
use App\Database\Relations\Relation;
use App\Domain\Organization\OrganizationModel;
use App\Domain\Person\PersonModel;
use App\Domain\User\UserModel;

class DealModel extends BaseModel
{
    const OWNER_COLUMN = 'user_id';
    const TABLE = 'deals';
    protected $table = self::TABLE;

    protected static $oneToOneRelations = [];
    protected static $oneToManyRelations = [];
    protected static $virtualColumns = [];

    protected static function bootRelationsTrait()
    {
        static::setOneToOneRelations([
            Relation::getBuilder(static::TABLE, 'person', PersonModel::class, 'id', 'person_id')->build(),
            Relation::getBuilder(static::TABLE, 'organization', OrganizationModel::class, 'id', 'org_id')->build(),
            Relation::getBuilder(static::TABLE, 'owner', UserModel::class, 'id', 'user_id')->build(),
        ]);
    }

    protected static function bootVirtualColumnsTrait()
    {
        static::setVirtualColumns([
            new ColumnReferenceVirtualColumn(
                'person_name',
                Relation::getBuilder(static::TABLE, 'person', PersonModel::class, 'id', 'person_id')
                    ->setModelClass(PersonModel::class)
                    ->setExtractable(false)
                    ->build(),
                'name'
            ),
            new RawVirtualColumn('now_time', 'current_timestamp()')
        ]);
    }
}