<?php

namespace App\Domain\Activity;


use App\AbstractEntityListController;
use App\AbstractEntityListService;
use App\Database\BaseModel;
use App\Domain\User\UserService;
use App\Field\FieldValidatorService;
use App\Filter\FilterNodesService;

class ActivityListController extends AbstractEntityListController
{
    private $activityService;

    public function __construct(
        FilterNodesService $filterService,
        FieldValidatorService $fieldValidatorService,
        UserService $userService,
        ActivityListService $activityListService
    )
    {
        parent::__construct($filterService, $fieldValidatorService, $userService);
        $this->activityService = $activityListService;
    }

    protected function getEntityService(): AbstractEntityListService
    {
        return $this->activityService;
    }

    protected function getEntityModelInstance(): BaseModel
    {
        return new ActivityModel();
    }
}