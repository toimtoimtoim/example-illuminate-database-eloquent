<?php

namespace App\Domain\Activity;

use App\Database\BaseModel;
use App\Database\Relations\Relation;
use App\Domain\Activity\Participant\ActivityParticipantModel;
use App\Domain\Deal\DealModel;
use App\Domain\Organization\OrganizationModel;
use App\Domain\Person\PersonModel;
use App\Domain\User\UserModel;

class ActivityModel extends BaseModel
{
    const OWNER_COLUMN = 'user_id';
    const TABLE = 'activity';
    protected $table = self::TABLE;

    protected static $oneToOneRelations = [];
    protected static $oneToManyRelations = [];

    protected static function bootRelationsTrait() {
        static::setOneToOneRelations([
            Relation::getBuilder(static::TABLE, 'person', PersonModel::class, 'id', 'person_id')->build(),
            Relation::getBuilder(static::TABLE, 'deal', DealModel::class, 'id', 'deal_id')->build(),
            Relation::getBuilder(static::TABLE, 'organization', OrganizationModel::class, 'id', 'org_id')->build(),
            Relation::getBuilder(static::TABLE, 'owner', UserModel::class, 'id', 'user_id')->build(),
        ]);

        static::setOneToManyRelations([
            Relation::getBuilder(static::TABLE, 'participants', ActivityParticipantModel::class, 'id', 'id')
                ->setExtractable(false)
                ->setPermission(null)
                ->setIsComplex(true)
                ->build()
        ]);
    }

    public function person() {
        return $this->hasOne(PersonModel::class, 'id', 'person_id');
    }

    public function participants()
    {
        return $this->hasMany(ActivityParticipantModel::class, 'activity_id');
    }

    public function participants_full()
    {
        return $this->belongsToMany(PersonModel::class, ActivityParticipantModel::TABLE, 'activity_id', 'person_id');
    }
}