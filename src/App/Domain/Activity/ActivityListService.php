<?php

namespace App\Domain\Activity;

use App\AbstractEntityListService;
use App\Database\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class ActivityListService extends AbstractEntityListService
{
    protected function getModelInstance()
    {
        return new ActivityModel();
    }
}