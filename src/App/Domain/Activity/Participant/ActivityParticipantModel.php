<?php

namespace App\Domain\Activity\Participant;


use App\Database\BaseModel;
use App\Domain\Activity\ActivityModel;

class ActivityParticipantModel extends BaseModel
{
    const TABLE = 'activity_participants';
    protected $table = self::TABLE;


    public function activity()
    {
        return $this->belongsTo(ActivityModel::class, 'id', 'activity_id');
    }
}