<?php

namespace App\Field;


final class FieldUtils
{
    private function __construct()
    {
        // utils class
    }

    public static function subStringAfterLastDot($fieldName)
    {
        $lastDotPosition = strrpos($fieldName, '.');
        return $lastDotPosition === false ? $fieldName : substr($fieldName, $lastDotPosition + 1);
    }

    public static function getFieldPartForFirstDot($fieldName)
    {
        $firstDotLocation = strpos($fieldName, '.');
        if (false === $firstDotLocation) {
            return [$fieldName, null];
        }
        return [
            substr($fieldName, 0, $firstDotLocation),
            substr($fieldName, $firstDotLocation + 1)
        ];
    }

}