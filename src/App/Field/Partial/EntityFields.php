<?php

namespace App\Field\Partial;


class EntityFields
{
    /**
     * @var string[]
     */
    private $fields = [];
    /**
     * @var EntityFields[]
     */
    private $subEntities = [];

    /**
     * @var string
     */
    private $name;

    /**
     * @var EntityFields parent field
     */
    private $parent;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function addField($field)
    {
        // add check - can not add field when subEntity exists
        $this->fields[] = $field;
        return $this;
    }

    public function addSubEntity(EntityFields $subEntity)
    {
        // add check - can not add sub entity when field exists
        $subEntity->setParent($this);
        $this->subEntities[$subEntity->getName()] = $subEntity;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        $result = $this->fields;
        foreach ($this->subEntities as $entity) {
            $result[] = $entity->getName() . ':[' . $entity . ']';
        }
        return implode(',', $result);
    }

    /**
     * @return EntityFields[]
     */
    public function getSubEntities(): array
    {
        return $this->subEntities;
    }

    /**
     * @return \string[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return \string[]
     */
    public function getFieldsForSelect(): array
    {
        $result = array_map(function ($field) {
            return $this->name . '.' . $field;
        }, $this->fields);

        return $result;
    }

    protected function setParent(EntityFields $parent)
    {
        $this->parent = $parent;
    }

    public function getParentPath()
    {
        $result = null;
        if ($this->parent) {
            $parentPath = $this->parent->getParentPath();
            if ($parentPath !== null) {
                $result = $parentPath . '.' . $this->getName();
            } else {
                $result = $this->getName();
            }
        }
        return $result;
    }

    public static function getForSelectAll($name)
    {
        return (new EntityFields($name))->addField('*');
    }

}