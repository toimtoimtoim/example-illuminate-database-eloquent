<?php

namespace App\Field\Partial;

class FieldParser
{
    /**
     * Parses string to nested structure of EntityField class
     *
     * Example: 'name,age,owner(name)' =>  [fields=[name,age], subEntities=[ owner:[fields=[name] ]]
     *
     * @param $str string to be parsed
     * @return EntityFields|null
     */
    public static function parse($str, $entityName = '')
    {
        if (empty($str)) {
            return null;
        }
        return static::parseInternal($entityName, str_split(strtolower($str)));
    }

    /**
     * Modified version of http://yaoganglian.com/2013/07/01/partial-response/
     */
    private static function parseInternal($field, array $chars, &$i = 0)
    {
        $fields = new EntityFields($field);
        $field = '';
        $length = count($chars);
        while ($i < $length) {
            $c = $chars[$i];
            $i++;

            if ($c === '(') {
                $fields->addSubEntity(static::parseInternal($field, $chars, $i));
                $field = '';
                continue;
            } elseif ($c === ')') {
                if ($field !== '') {
                    $fields->addField($field);
                }
                return $fields;
            }

            if ($c === '_' || ctype_alnum($c)) {
                $field .= $c;
            } elseif ($field !== '') {
                $fields->addField($field);
                $field = '';
            }
        }
        if ($field !== '') {
            $fields->addField($field);
        }
        return $fields;
    }

}