<?php

namespace App\Field;

use App\AppValidationException;
use App\Database\BaseModel;
use App\Field\Partial\EntityFields;
use Illuminate\Support\Collection;

class FieldValidatorService
{
    /**
     * @var FieldService
     */
    private $fieldService;

    public function __construct(FieldService $fieldService)
    {
        $this->fieldService = $fieldService;
    }

    /**
     * @param EntityFields $fields
     * @param BaseModel $model
     *
     * @throws \App\AppValidationException
     */
    public function validate(EntityFields $fields, BaseModel $model)
    {
        $actualFields = $this->fieldService->getFields($model->getTable())->pluck('column');
        foreach ($model::getVirtualColumns() as $column) {
            $actualFields->push($column->getName());
        }

        $relationColumns = new Collection();
        foreach ($model::getAllRelations() as $relation) {
            $relationColumns->put($relation->getFieldName(), $relation->getModelInstance());
        }

        foreach ($fields->getFields() as $field) {
            if (!$actualFields->contains($field) && !$relationColumns->has($field)) {
                throw new AppValidationException(["field '{$field}' does not exists on entity"]);
            }
        }

        foreach ($fields->getSubEntities() as $entity) {
            $entityFieldName = $entity->getName();
            $subModel = $relationColumns->get($entityFieldName);
            if ($subModel === null) {
                throw new AppValidationException(["field '{$entityFieldName}' does not exists on entity"]);
            }
            $this->validate($entity, $subModel);
        }
    }

}