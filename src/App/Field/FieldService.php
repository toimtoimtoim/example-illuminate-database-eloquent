<?php

namespace App\Field;


use Illuminate\Database\Capsule\Manager;

class FieldService
{
    /**
     * @var Manager
     */
    private $db;

    public function __construct(Manager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $table
     * @return \Illuminate\Support\Collection
     */
    public function getFields($table)
    {
        // TODO should cache these results
        return $this->db->getConnection()->query()
            ->select(['column_name as column', 'data_type as type'])
            ->from('INFORMATION_SCHEMA.COLUMNS')
            ->where('table_name', '=', $table)
            ->whereRaw('table_schema = database()')
            ->orderBy('column_name')
            ->get();
    }

}