<?php

namespace App\Field;

use App\Constants;
use App\Database\BaseModel;
use App\Database\Column\Column;
use App\Database\Column\DefaultColumn;
use App\Database\Column\SelectColumnUtils;
use App\Database\EagerLoad;
use App\Database\QueryParts;
use App\Database\Relations\Relation;
use App\Field\Partial\EntityFields;

/**
 * Class responsible for extracting columns for entity fields, joined relations and one-to-many relations eager loading if needed
 */
class FieldsToQueryPartsProcessor
{
    /**
     * @var int
     */
    private $maxNestingLevel;

    /**
     * @var Column[]
     */
    private $selectColumns;

    /**
     * @var EagerLoad[]
     */
    private $eagerLoadedRelations;

    /**
     * @var Relation[]
     *
     */
    private $joinedRelations;

    public function __construct($maxNestingLevel = Constants::PARTIAL_RESPONSE_FIELDS_MAX_NESTING_LEVEL)
    {
        $this->maxNestingLevel = $maxNestingLevel;
    }

    public function process(BaseModel $currentModel, ?EntityFields $fields): QueryParts
    {
        $this->selectColumns = [];
        $this->joinedRelations = [];
        $this->eagerLoadedRelations = [];

        $this->processField($currentModel, $fields);

        return new QueryParts($this->selectColumns, $this->joinedRelations, $this->eagerLoadedRelations);
    }

    /**
     * @param BaseModel $currentModel
     * @param EntityFields|null $fields
     * @param int $nestingLevel
     */
    private function processField(BaseModel $currentModel, ?EntityFields $fields, $nestingLevel = 0)
    {
        if ($nestingLevel >= $this->maxNestingLevel) {
            return;
        }

        $this->gatherOneToOneRelations($currentModel, $fields, $nestingLevel + 1);
        $this->gatherOneToManyRelations($currentModel, $fields, $nestingLevel + 1);
    }

    private function gatherOneToManyRelations(BaseModel $currentModel, EntityFields $fields, $nestingLevel = 0)
    {
        if ($nestingLevel > $this->maxNestingLevel) {
            return;
        }

        $partialRelations = $fields->getSubEntities();

        foreach ($currentModel::getOneToManyRelations() as $relation) {
            $fieldName = $relation->getFieldName();

            // if one-to-many relation was mentioned in partial response field list remove it from selectable columns
            // as this field does not exist on entity as column - it will be joined and hydrated later
            $this->removeSelectColumns($fields->getName() . '.' . $fieldName);

            $fieldsForSelect = $fields ? $fields->getFields() : [];
            $partialResponse = $partialRelations[$fieldName] ?? null;
            if ($partialResponse || in_array($fieldName, $fieldsForSelect, true)) {
                $this->eagerLoadedRelations[] = new EagerLoad($relation, $fields->getParentPath(), $partialResponse);
            }
        }
    }

    private function gatherOneToOneRelations(BaseModel $currentModel, EntityFields $fields, $nestingLevel = 0)
    {
        if ($nestingLevel > $this->maxNestingLevel) {
            return;
        }

        $partialRelations = $fields->getSubEntities();
        $selectColumns = SelectColumnUtils::extractColumnsForSelect($fields->getName(), $fields->getFieldsForSelect(), $currentModel::getVirtualColumns());

        // one to one relations are joined immediately
        foreach ($currentModel::getOneToOneRelations() as $relation) {
            $fieldNameWithTable = $relation->getFieldNameWithTable();
            $relationRequestedAsFieldKey = array_key_exists($fieldNameWithTable, $selectColumns);

            $fieldName = $relation->getFieldName();
            $isRelationRequestPartially = isset($partialRelations[$fieldName]);
            if ($isRelationRequestPartially) {
                $this->processField($relation->getModelInstance(), $partialRelations[$fieldName], $nestingLevel);
            } elseif ($partialRelations === null || $relationRequestedAsFieldKey) {

                if ($relationRequestedAsFieldKey !== false) {
                    // remove relation name as this field does not exists on model table
                    unset($selectColumns[$fieldNameWithTable]);
                }
                $selectColumns[$fieldNameWithTable] = new DefaultColumn($relation->getSelectColumns());
            }

            if ($partialRelations === null || $isRelationRequestPartially || $relationRequestedAsFieldKey) {
                $this->joinedRelations[] = $relation;
            }
        }
        $this->addSelectColumns($selectColumns);
    }

    private function addSelectColumns(array $columns = null)
    {
        if ($columns) {
            $this->selectColumns = array_merge($this->selectColumns, $columns);
        }
    }

    private function removeSelectColumns($name)
    {
        if ($name) {
            $this->selectColumns = array_diff_key($this->selectColumns, [$name => null]);
        }
    }
}