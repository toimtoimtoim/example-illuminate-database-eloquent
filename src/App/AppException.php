<?php

namespace App;

use RuntimeException;

/**
 * Base class for ALL application errors should extend
 */
class AppException extends RuntimeException
{

}