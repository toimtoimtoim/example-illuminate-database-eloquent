<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
use App\Constants;
use App\Middleware\AuthenticationFilter;
use App\Middleware\SqlQueryResponseFilter;

/**
 * Middleware is executed in LIFO (last in first out) order.
 */
$app->add(new AuthenticationFilter($app->getContainer()));

if (Constants::ENV_DEV === env('ENV', Constants::ENV_LIVE)) {
    $app->add(new SqlQueryResponseFilter($app->getContainer()));
}
