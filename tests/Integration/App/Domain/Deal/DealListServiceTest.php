<?php

namespace Tests\Integration\App\Domain\Deal;


use DateTime;
use Tests\Integration\AbstractIntegrationTestCase;

class DealListServiceTest extends AbstractIntegrationTestCase
{
    public function testDealListPartialResponse()
    {
        $response = $this->getApp()
            ->setQueryString('&filter="id=1"&fields=id,person_id,person(id,name),organization(id),user_id')
            ->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].user_id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].id', 1);
    }

    public function testDealListPartialResponseWithRawVirtualColumn()
    {
        $response = $this->getApp()->setQueryString('&filter="id=1"&fields=id,now_time,person_id,person(id,name),organization(id),user_id')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].user_id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].id', 1);

        $date = new DateTime();
        $this->assertJsonValueMatches($content, '$.data[0].now_time', $this->stringContains($date->format('Y-m-d')));
    }

    public function testDealListPartialResponseWithColumnReferenceVirtualColumn()
    {
        $response = $this->getApp()->setQueryString('&filter="id=1"&fields=id,person_name')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0]', [
            'id' => 1,
            'person_name' => 'uus person', // must contain 'person_name' and not 'person_id' as we are not extracting currently
        ]);
    }

    public function testDealListPartialResponseByFilterId()
    {
        $response = $this->getApp()->setQueryString('&filter_id=1&fields=id,person_id,person(id,name),organization(id),user_id')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(2));
    }

    public function testFilterByFieldNotInPartialRequest()
    {
        $response = $this->getApp()->setQueryString('&filter_id=2&fields=id,person_id,user_id')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0].id', 2);
        $this->assertJsonValueMatches($content, '$.related_objects.person', $this->countOf(0));
    }

    public function testFilterByFieldNotInPartialRequestWithRegularUser()
    {
        $response = $this->getApp()
            ->setApiToken(static::REGULAR_USER_API_TOKEN)
            ->setQueryString('&filter="person.name=asd"&fields=id,person_id,user_id')
            ->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0].id', 3);
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 4);
        $this->assertJsonValueMatches($content, '$.related_objects.person', $this->countOf(0));
    }

    public function testDealListPartialResponseWithOneToOneRelationAsStarSelect()
    {
        $response = $this->getApp()->setQueryString('&filter_id=1&fields=id,person_id,person,organization(id),user_id')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(2));

        $this->assertJsonValueEquals($content, '$.data[0]', [
            'id' => 2,
            'person_id' => 2,
            'user_id' => 1,
            'org_id' => 2 // one-to-one relation foreign key must be added automatically so relation could be found from related_objects
        ]);

        $this->assertJsonValueMatches($content, '$.related_objects.person', $this->countOf(2));
        $this->assertJsonValueMatches($content, '$.related_objects.person[1]', $this->countOf(42)); // all fields as specified in partial request

        $this->assertJsonValueMatches($content, '$.related_objects.organization', $this->countOf(2));
        $this->assertJsonValueMatches($content, '$.related_objects.organization[1]', $this->countOf(1)); // only 1 field as specified in partial request
    }

    public function testDealListRegularUserSeesOwnedAndFollowedDeals()
    {
        $response = $this->getApp()
            ->setApiToken(static::REGULAR_USER_API_TOKEN)
            ->setQueryString('&fields=id,person_id,person(id,name),organization(id),user_id')
            ->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(2));

        $this->assertJsonValueEquals($content, '$.data[0].id', 3);
        $this->assertJsonValueEquals($content, '$.data[0].user_id', 4);
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 4);

        $this->assertJsonValueEquals($content, '$.data[1].id', 2);
        $this->assertJsonValueEquals($content, '$.data[1].user_id', 1);
        $this->assertJsonValueEquals($content, '$.data[1].person_id', 2);

        $this->assertJsonValueMatches($content, '$.related_objects.person', $this->countOf(1));
        $this->assertJsonValueMatches($content, '$.related_objects.person[4]', $this->countOf(2)); // person.id=2 is owned by regular user

        $this->assertJsonValueMatches($content, '$.related_objects.organization', $this->countOf(1));
        $this->assertJsonValueMatches($content, '$.related_objects.organization[2]', $this->countOf(1)); // org.id=2 is owned by regular user
    }

    public function testDealListWithThirdLevelRelationAsStarSelect()
    {
        $response = $this->getApp()->setQueryString('&filter="id=1"&fields=id,person(id,name,pictures)')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures', $this->countOf(2));

        // pictures has 7 columns
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures[1]', $this->countOf(7));
    }

    public function testDealListWithThirdLevelRelationAsPartialRequest()
    {
        $response = $this->getApp()->setQueryString('&filter="id=1"&fields=id,person(id,name,pictures(id,active_flag))')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures', $this->countOf(2));

        // requested 2 columns for pictures, additional 1 is added automatically
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures[1]', $this->countOf(3));
        $this->assertJsonValueEquals($content, '$.related_objects.person[1].pictures[1]', [
            'id' => 2,
            'item_id' => 1,
            'active_flag' => 0
        ]);
    }

    public function testDealListWithComplexFilter()
    {
        $response = $this->getApp()->setQueryString('&filter="person.pictures.id=1"&fields=id,person_id,person(id,name,pictures)')->get('/deal');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);

        $this->assertTrue(is_array($content));

        $this->assertJsonValueMatches($content, '$.data', $this->countOf(1));
        $this->assertJsonValueEquals($content, '$.data[0]', [
            'id' => 1,
            'person_id' => 1
        ]);
        $this->assertJsonValueMatches($content, '$.related_objects.person', $this->countOf(1));

        $this->assertJsonValueMatches($content, '$.related_objects.person[1]', $this->countOf(3));
        $this->assertJsonValueEquals($content, '$.related_objects.person[1].id', 1);
        $this->assertJsonValueEquals($content, '$.related_objects.person[1].name', 'uus person');

        // TODO should pictures be filtered by id=1? probably
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures', $this->countOf(2));
        $this->assertJsonValueMatches($content, '$.related_objects.person[1].pictures[0]', $this->countOf(7));
    }

}