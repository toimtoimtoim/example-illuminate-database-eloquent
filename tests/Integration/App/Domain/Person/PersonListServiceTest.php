<?php

namespace Tests\Integration\App\Domain\Person;


use Tests\Integration\AbstractIntegrationTestCase;

class PersonListServiceTest extends AbstractIntegrationTestCase
{
    public function testListPartialResponseWithOneToManyRelationIncludedWithStarSelect()
    {
        $response = $this->getApp()
            ->setQueryString('&filter="id=1"&fields=id,org_id,pictures,owner_id')
            ->get('/person');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);
        $this->assertJsonValueEquals($content, '$.data[0].id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].org_id', 1);

        // pictures is one-to-many relation on person entity
        $this->assertJsonValueMatches($content, '$.data[0].pictures', $this->countOf(2));
        // when extracted only ids remain
        $this->assertJsonValueEquals($content, '$.data[0].pictures', [1,2]);

        $this->assertJsonValueMatches($content, '$.related_objects.pictures', $this->countOf(2));
        // extracted one-to-many has all fields asked (this time all fields are we did star select)
        $this->assertJsonValueMatches($content, '$.related_objects.pictures[1]', $this->countOf(7));

        $this->assertJsonValueEquals($content, '$.related_objects.pictures[1].id', 1);
        $this->assertJsonValueEquals($content, '$.related_objects.pictures[2].item_id', 1);

        $this->assertJsonValueEquals($content, '$.related_objects.pictures[2].id', 2);
        $this->assertJsonValueEquals($content, '$.related_objects.pictures[2].item_id', 1);
    }

}