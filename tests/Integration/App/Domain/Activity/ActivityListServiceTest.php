<?php

namespace Tests\Integration\App\Domain\Activity;


use Tests\Integration\AbstractIntegrationTestCase;

class ActivityListServiceTest extends AbstractIntegrationTestCase
{
    public function testActivityList()
    {
        $response = $this->getApp()
            ->get('/activity');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);
        $this->assertJsonValueMatches($content, '$.data', $this->countOf(4));

        $this->assertJsonValueEquals($content, '$.data[0].id', 4);
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 4);

        // check response schema
//        $this->assertJsonDocumentMatchesSchema($content, [
//            'type'       => 'object',
//            'required'   => ['username', 'age'],
//            'properties' => [
//                'username' => ['type' => 'string', 'minLength' => 3],
//                'age'      => ['type' => 'number']
//            ]
//        ]);
    }

    public function testActivityListPartialResponse()
    {
        $response = $this->getApp()
            ->setQueryString('&filter="id=1"&fields=id,person_id,person(id,name,pictures),user_id')
            ->get('/activity');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);
        $this->assertJsonValueEquals($content, '$.data[0].id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 4);

        $this->assertJsonValueEquals($content, '$.related_objects[\'person\'][4].id', 4);
        $this->assertJsonValueMatches($content, '$.related_objects[\'person\'][4]', $this->countOf(3));
    }

    public function testActivityListPartialResponseWithOneToManyRelationIncluded()
    {
        $response = $this->getApp()
            ->setQueryString('&filter="id=1"&fields=id,person_id,person(id,name,pictures),participants(person_id),user_id')
            ->get('/activity');

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getBody(), true);
        $this->assertJsonValueEquals($content, '$.data[0].id', 1);
        $this->assertJsonValueEquals($content, '$.data[0].person_id', 4);
        $this->assertJsonValueMatches($content, '$.data[0].participants', $this->countOf(3));

        $this->assertJsonValueEquals($content, '$.data[0].participants[0]', [
            'activity_id' => 1,
            'person_id' => 1
        ]);
        $this->assertJsonValueEquals($content, '$.data[0].participants[2]', [
            'activity_id' => 1,
            'person_id' => 4
        ]);
    }

}