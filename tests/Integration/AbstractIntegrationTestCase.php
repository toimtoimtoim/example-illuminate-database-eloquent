<?php

namespace Tests\Integration;

use Helmich\JsonAssert\JsonAssertions;
use PHPUnit\Framework\TestCase;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
abstract class AbstractIntegrationTestCase extends Testcase
{
    use JsonAssertions;

    const ADMIN_API_TOKEN = 'TOKEN_FOR_LOCAL_TEST';
    const REGULAR_USER_API_TOKEN = 'TOKEN_FOR_FOLLOWER_USER';
    /**
     * Process the application given a request method and URI
     *
     * @param null $method
     * @param null $uri
     * @return TestApp
     */
    public function getApp($method = null, $uri = null)
    {
        $testApp = new TestApp($method, $uri);
        $testApp->setApiToken(static::ADMIN_API_TOKEN);
        return $testApp;
    }
}