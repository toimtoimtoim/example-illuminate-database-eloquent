<?php

namespace Tests\Integration;


use App\Constants;
use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

class TestApp
{
    private $method;
    private $requestData;
    private $uri;
    private $queryString;
    private $headers;
    /**
     * by default lets run every request with regular user api token
     */
    private $apiToken;
    private $envParams;

    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = true;


    public function __construct($method = null, $uri = null, $requestData = null, array $envParams = [])
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->requestData = $requestData;
        $this->envParams = $envParams;
    }

    public function get($uri)
    {
        $this->method = 'GET';
        $this->uri = $uri;
        return $this->execute();
    }

    public function post($uri)
    {
        $this->method = 'POST';
        $this->uri = $uri;
        return $this->execute();
    }

    public function put($uri)
    {
        $this->method = 'PUT';
        $this->uri = $uri;
        return $this->execute();
    }

    public function delete($uri)
    {
        $this->method = 'DELETE';
        $this->uri = $uri;
        return $this->execute();
    }

    public function execute()
    {
        // Create a mock environment for testing with
        $environment = Environment::mock(
            array_merge(
                $this->envParams,
                [
                    'REQUEST_METHOD' => $this->method,
                    'REQUEST_URI' => $this->uri,
                    'QUERY_STRING' => $this->queryString,
                ]
            )
        );

        $apiToken = $environment->get('API_TOKEN', $this->apiToken);

        if ($apiToken) {
            $queryString = $environment->get('QUERY_STRING');
            $environment->set('QUERY_STRING', $queryString . '&API_TOKEN=' . $apiToken);
        }

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if ($this->requestData !== null) {
            $request = $request->withParsedBody($this->requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';
        $settings['settings'][Constants::DB_INSTANCE]['host'] = '127.0.0.1';
        $settings['settings'][Constants::DB_INSTANCE]['port'] = '33060';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__ . '/../../src/dependencies.php';

        // Register middleware
        if ($this->withMiddleware) {
            require __DIR__ . '/../../src/middleware.php';
        }

        // Register routes
        require __DIR__ . '/../../src/routes.php';

        // Process the request in application
        $response = $app->process($request, $response);

        return $response;
    }

    /**
     * @param mixed $method
     * @return TestApp
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param null $requestData
     * @return TestApp
     */
    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;
        return $this;
    }

    /**
     * @param mixed $queryString
     * @return TestApp
     */
    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
        return $this;
    }

    /**
     * @param mixed $headers
     * @return TestApp
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param mixed $apiToken
     * @return TestApp
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * @param array $envParams
     * @return TestApp
     */
    public function setEnvParams(array $envParams): TestApp
    {
        $this->envParams = $envParams;
        return $this;
    }

}