<?php

namespace Tests\unit\App;

use App\Domain\Deal\DealModel;
use App\Domain\Organization\OrganizationModel;
use App\Domain\Person\PersonModel;
use App\Domain\Picture\PictureModel;
use App\RelatedObjectsExtractor;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\TestCase;

class RelatedObjectsExtractorTest extends TestCase
{
    public function testExtractionWithOutUnsettingItems()
    {
        $collection = $this->getTestCollection();

        $relatedObjects = RelatedObjectsExtractor::extract($collection);

        $firstDeal = $collection->first();
        $this->assertNotNull($firstDeal->organization);
        $this->assertNotNull($firstDeal->person);

        $this->assertCount(3, $relatedObjects);
        $this->assertCount(0, $relatedObjects->get('owner'));
        $this->assertCount(2, $relatedObjects->get('organization'));
        $this->assertEquals([20, 21], $relatedObjects->get('organization')->pluck('id')->toArray());
        $this->assertCount(1, $relatedObjects->get('person'));
        $this->assertEquals([30], $relatedObjects->get('person')->pluck('id')->toArray());
    }

    public function testExtractionWithUnsettingItems()
    {
        $collection = $this->getTestCollection();

        $relatedObjects = RelatedObjectsExtractor::extract($collection, true);

        $firstDeal = $collection->first();
        $this->assertNull($firstDeal->organization);
        $this->assertNull($firstDeal->person);

        $this->assertCount(3, $relatedObjects);
        $this->assertCount(0, $relatedObjects->get('owner'));
        $this->assertCount(2, $relatedObjects->get('organization'));
        $this->assertEquals([20, 21], $relatedObjects->get('organization')->pluck('id')->toArray());
        $this->assertCount(1, $relatedObjects->get('person'));
        $this->assertEquals([30], $relatedObjects->get('person')->pluck('id')->toArray());
    }

    public function testExtractionOneToManyRelations()
    {
        $collection = $this->getOneToManyRelationCollection();

        $relatedObjects = RelatedObjectsExtractor::extract($collection, true);

        $this->assertCount(2, $collection);

        $firstPerson = $collection->first();
        $this->assertNotNull($firstPerson->pictures);

        $this->assertCount(3, $relatedObjects);
        $this->assertCount(0, $relatedObjects->get('owner'));
        $this->assertCount(3, $relatedObjects->get('pictures'));
        $this->assertEquals([1, 2, 3], $relatedObjects->get('pictures')->pluck('id')->toArray());

        $this->assertCount(1, $relatedObjects->get('organization'));
        $this->assertEquals([21], $relatedObjects->get('organization')->pluck('id')->toArray());
    }

    private function getTestCollection(): Collection
    {
        $org1 = new OrganizationModel();
        $org1->id = 20;
        $org1->name = 'org1';

        $person1 = new PersonModel();
        $person1->id = 30;
        $person1->name = 'person1';


        $model1 = new DealModel();
        $model1->id = 1;
        $model1->org_id = 2;
        $model1->setRelation('organization', $org1);
        $model1->setRelation('person', $person1);

        $org2 = new OrganizationModel();
        $org2->id = 21;
        $org2->name = 'org2';

        $model2 = new DealModel();
        $model2->id = 1;
        $model2->org_id = 2;
        $model2->setRelation('organization', $org2);
        $model2->setRelation('person', $person1);

        return new Collection([$model1, $model2]);
    }

    private function getOneToManyRelationCollection()
    {
        $picture1 = new PictureModel();
        $picture1->id = 1;
        $picture1->item_id = 1;

        $picture2 = new PictureModel();
        $picture2->id = 2;
        $picture2->item_id = 1;

        $picture3 = new PictureModel();
        $picture3->id = 3;
        $picture3->item_id = 2;

        $person1 = new PersonModel();
        $person1->id = 1;
        $person1->name = 'person1';
        $person1->setRelation('pictures', new Collection([$picture1, $picture2]));

        $org1 = new OrganizationModel();
        $org1->id = 21;
        $org1->name = 'org1';
        $person1->setRelation('organization', $org1);

        $person2 = new PersonModel();
        $person2->id = 2;
        $person2->name = 'person2';
        $person2->setRelation('pictures', new Collection([$picture3]));

        return new Collection([$person1, $person2]);
    }

}