<?php

namespace Tests\App\Filter\Node;


use App\Domain\Filter\FilterConditionModel;
use App\Filter\Node\BinaryExpressionNode;
use App\Filter\Node\FilterConditionNode;
use App\Filter\Node\ConditionsToNodesConverter;
use App\Filter\Node\FieldNode;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class ConditionsToNodesConverterTest extends TestCase
{
    public function testSuccess() {
        $rootCondition = new FilterConditionModel([
            'operator' => 'OR'
        ]);
        $rootCondition->id = 1;

        $dealOr = new FilterConditionModel([
            'operator' => 'OR',
            'parent_id' => $rootCondition->id,
        ]);
        $dealOr->id = 2;

        $dealEq1 = new FilterConditionModel([
            'operator' => '=',
            'parent_id' => $dealOr->id,
            'relation' => null,
            'field_id' => 'id',
            'value' => '1',
        ]);
        $dealEq1->id = 3;

        $dealEq2 = new FilterConditionModel([
            'operator' => '=',
            'parent_id' => $dealOr->id,
            'relation' => null,
            'field_id' => 'id',
            'value' => '1',
        ]);
        $dealEq2->id = 4;


        $dealEqOwnerId = new FilterConditionModel([
            'operator' => '=',
            'parent_id' => $rootCondition->id,
            'relation' => null,
            'field_id' => 'owner_id',
            'value' => '10',
        ]);
        $dealEqOwnerId->id = 5;

        $conditions = new Collection([
            $rootCondition,
            $dealOr,
            $dealEq1,
            $dealEq2,
            $dealEqOwnerId
        ]);

        $converter = new ConditionsToNodesConverter($conditions);

        $rootNode = $converter->convert();

        $this->assertEquals('OR', $rootNode->getAttribute('operator'));

        $dealOrNode = $rootNode->getNode('left');
        $this->assertInstanceOf(BinaryExpressionNode::class, $dealOrNode);
        $this->assertEquals('OR', $dealOrNode->getAttribute('operator'));

        $dealEq1Node = $dealOrNode->getNode('left');
        $this->assertInstanceOf(FilterConditionNode::class, $dealEq1Node);
        $this->assertEquals('=', $dealEq1Node->getOperator());
        $this->assertEquals('id', $dealEq1Node->getField());
        $this->assertEquals('1', $dealEq1Node->getValue());

        $dealEq2Node = $dealOrNode->getNode('right');
        $this->assertInstanceOf(FilterConditionNode::class, $dealEq2Node);
        $this->assertEquals('=', $dealEq2Node->getOperator());
        $this->assertEquals('id', $dealEq2Node->getField());
        $this->assertEquals('1', $dealEq2Node->getValue());

        // owner nodes
        $dealOwnerNode = $rootNode->getNode('right');
        $this->assertInstanceOf(FilterConditionNode::class, $dealOwnerNode);
        $this->assertEquals('=', $dealOwnerNode->getOperator());
        $this->assertEquals('owner_id', $dealOwnerNode->getField());
        $this->assertEquals('10', $dealOwnerNode->getValue());
    }

}