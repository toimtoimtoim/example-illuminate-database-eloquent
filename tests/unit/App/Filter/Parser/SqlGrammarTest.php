<?php

namespace Tests\App\Filter\Parser;

use App\Filter\Parser\SqlLexer;
use App\Filter\Parser\SqlStringGrammar;
use Dissect\Parser\LALR1\Parser;
use PHPUnit\Framework\TestCase;

class SqlGrammarTest extends TestCase
{
    private $lexer;
    private $parser;
    private $grammar;


    protected function setUp()
    {
        $this->lexer = new SqlLexer();
        $this->grammar = new SqlStringGrammar();
        $this->parser = new Parser($this->grammar);
    }

    /**
     * @expectedException \Dissect\Parser\Exception\UnexpectedTokenException
     */
    public function testFailSyntax()
    {
        $stream = $this->lexer->lex('deal.id > 1 > 1');
        $this->parser->parse($stream);
    }

    public function testPass0()
    {
        $stream = $this->lexer->lex('deal.id > 1 && (person.id >= tere || activity.id)');
        $this->assertEquals('AND(>(deal.id,1),OR(>=(person.id,tere),activity.id))', $this->parser->parse($stream));
    }

    public function testPass()
    {
        $stream = $this->lexer->lex('deal.id && (person.id || activity.id)');
        $this->assertEquals('AND(deal.id,OR(person.id,activity.id))', $this->parser->parse($stream));
    }

    public function testPass2()
    {
        $stream = $this->lexer->lex('deal.id = 1 || (person.id=1 && activity.id =2)');
        $this->assertEquals('OR(=(deal.id,1),AND(=(person.id,1),=(activity.id,2)))', $this->parser->parse($stream));
    }

    public function testPass3()
    {
        $stream = $this->lexer->lex('deal.id = 1 || activity.id = 2');
        $this->assertEquals('OR(=(deal.id,1),=(activity.id,2))', $this->parser->parse($stream));
    }

    public function testPass4()
    {
        $stream = $this->lexer->lex('deal.id = 1 && activity.id = 2');
        $this->assertEquals('AND(=(deal.id,1),=(activity.id,2))', $this->parser->parse($stream));
    }

    public function testPassMultipleSameLevel()
    {
        $stream = $this->lexer->lex('deal.id = 2 || deal.id = 1 || activity.id = 2');
        $this->assertEquals('OR(OR(=(deal.id,2),=(deal.id,1)),=(activity.id,2))', $this->parser->parse($stream));
    }

    public function testPassMultipleSameLevel2()
    {
        $stream = $this->lexer->lex('deal.id = 2 || deal.id = 1 && activity.id = 2');
        $this->assertEquals('AND(OR(=(deal.id,2),=(deal.id,1)),=(activity.id,2))', $this->parser->parse($stream));
    }
}