<?php

namespace Tests\App\Filter\Parser;


use App\AppException;
use App\Database\CustomMySqlConnection;
use App\Domain\Deal\DealModel;
use App\Filter\Node\BinaryExpressionNode;
use App\Filter\Node\FieldNode;
use App\Filter\Node\FilterConditionNode;
use App\Filter\Node\NodeToEloquentQueryBuilder;
use App\Filter\Parser\SqlBuilderGrammar;
use App\Filter\Parser\SqlLexer;
use Dissect\Parser\LALR1\Parser;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use PHPUnit\Framework\TestCase;

class SqlBuilderGrammarTest extends TestCase
{
    private $lexer;
    private $parser;
    private $grammar;

    private $model;

    protected function setUp()
    {
        $this->model = new DealModel();

        $this->lexer = new SqlLexer();
        $this->grammar = new SqlBuilderGrammar();
        $this->parser = new Parser($this->grammar);


        $mock = $this->getMockBuilder(ConnectionResolver::class)->disableOriginalConstructor()
            ->setMethods(['connection'])
            ->getMock();

        $connection = $this->getMockBuilder(CustomMySqlConnection::class)->disableOriginalConstructor()
            ->setMethods(['getQueryGrammar'])->getMock();

        $connection->expects($this->any())->method('getQueryGrammar')
            ->willReturn(new MySqlGrammar());

        $mock->expects($this->any())->method('connection')
            ->willReturn($connection);

        DealModel::setConnectionResolver($mock);
    }


    public function testPass2()
    {
        $stream = $this->lexer->lex('id = 1 && (person.id=1 || organization.id =2)');
        $ast = $this->parser->parse($stream);

        $query = DealModel::query();
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);

        // we are looking for:
        // 1) persons is left joined as "person.id" is mentioned in filter
        // 2) org is left joined as "organization.id" is mentioned in filter
        // 3) person and org condition is in parenthesis
        $this->assertEquals('select `deals`.`person_id`, `deals`.`org_id` from `deals` left join `persons` as `person` on `deals`.`person_id` = `person`.`id` left join `org` as `organization` on `deals`.`org_id` = `organization`.`id` where `deals`.`id` = ? AND (`person`.`id` = ? OR `organization`.`id` = ?)', $query->toSql());
    }

    public function testPass3()
    {
        $stream = $this->lexer->lex('id = 1 || organization.id = 2');
        $ast = $this->parser->parse($stream);

        $query = DealModel::query()->select(['id']);
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);
        $this->assertEquals('select `id`, `deals`.`org_id` from `deals` left join `org` as `organization` on `deals`.`org_id` = `organization`.`id` where `deals`.`id` = ? OR `organization`.`id` = ?', $query->toSql());
    }

    public function testPass4()
    {
        $stream = $this->lexer->lex('id = 1 || id = 2 || person.id = 2');
        $ast = $this->parser->parse($stream);

        $query = DealModel::query()->select(['id']);
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);
        $this->assertEquals('select `id`, `deals`.`person_id` from `deals` left join `persons` as `person` on `deals`.`person_id` = `person`.`id` where (`deals`.`id` = ? OR `deals`.`id` = ?) OR `person`.`id` = ?', $query->toSql());
    }

    public function testPass6()
    {
        $activityIdEq2 = new FilterConditionNode(null, 'owner_id', '=', '1');
        $dealOr = new BinaryExpressionNode(
            new FilterConditionNode(null, 'id', '=', '1'),
            'OR',
            new FilterConditionNode(null, 'id', '=', '2')
        );

        $ast = new BinaryExpressionNode($dealOr, 'OR', $activityIdEq2);

        $query = DealModel::query()->select(['id']);
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);
        $this->assertEquals('select `id` from `deals` where (`deals`.`id` = ? OR `deals`.`id` = ?) OR `deals`.`owner_id` = ?', $query->toSql());
    }

    public function testJoinRelationByFilterCondition()
    {
        $stream = $this->lexer->lex('person.org_id = 2');
        $ast = $this->parser->parse($stream);

        $query = DealModel::query()->select(['id']);
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);
        $this->assertEquals('select `id`, `deals`.`person_id` from `deals` left join `persons` as `person` on `deals`.`person_id` = `person`.`id` where `person`.`org_id` = ?', $query->toSql());
    }

    /**
     * @expectedException \App\AppException
     * @expectedExceptionMessage Filter condition tries to join unknown relation for model: deals, relation: activity
     */
    public function testFailToJoinUnknownRelation()
    {
        $stream = $this->lexer->lex('activity.id = 2'); // deal does not have activity as relation
        $ast = $this->parser->parse($stream);

        $query = DealModel::query();
        NodeToEloquentQueryBuilder::applyToQuery($this->model, $ast, $query);
    }

}