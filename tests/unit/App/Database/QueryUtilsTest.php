<?php

namespace Tests\unit\App\Database;


use App\Database\CustomMySqlConnection;
use App\Database\QueryUtils;
use App\Domain\Deal\DealModel;
use Illuminate\Database\DatabaseManager;
use PHPUnit\Framework\TestCase;

class QueryUtilsTest extends TestCase
{
    public function testIsColumnSelected()
    {
        $query = $this->getQuery();
        $query->addSelect('deals.id');
        $query->addSelect('deals.user_id');

        $this->assertTrue(QueryUtils::isColumnSelected($query, 'deals.id'));
        $this->assertTrue(QueryUtils::isColumnSelected($query, 'deals.user_id'));

        $this->assertFalse(QueryUtils::isColumnSelected($query, 'deals.something'));
        $this->assertFalse(QueryUtils::isColumnSelected($query, ''));
        $this->assertFalse(QueryUtils::isColumnSelected($query, null));
    }

    public function testIsTableJoined()
    {
        $query = $this->getQuery();
        $query->leftJoin('deals', 'deal_id', '=', 'id');
        $query->leftJoin('persons as person', 'person_id', '=', 'id');

        $this->assertTrue(QueryUtils::isTableJoined($query, 'deals'));
        $this->assertTrue(QueryUtils::isTableJoined($query, 'persons as person'));

        $this->assertFalse(QueryUtils::isTableJoined($query, 'users'));
        $this->assertFalse(QueryUtils::isTableJoined($query, ''));
        $this->assertFalse(QueryUtils::isTableJoined($query, null));
    }

    private function getQuery()
    {
        $model = new DealModel();
        $resolverMock = $this->getMockBuilder(DatabaseManager::class)->setMethods(['connection'])->disableOriginalConstructor()->getMock();

        $resolverMock->expects($this->any())->method('connection')
            ->willReturn($this->getMockBuilder(CustomMySqlConnection::class)->disableOriginalConstructor()->getMock());

        $model::setConnectionResolver($resolverMock);

        return $model->query();
    }

}