<?php

namespace Tests\unit\App\Database;


use App\Database\ModelUtils;
use App\Domain\Activity\ActivityModel;
use App\Domain\Deal\DealModel;
use PHPUnit\Framework\TestCase;

class ModelUtilsTest extends TestCase
{
    private $model;

    public function setUp()
    {
        parent::setUp();
        $this->model = new DealModel();
    }

    public function testFindRelationByName()
    {
        $relation = ModelUtils::findRelationByFieldName($this->model, 'person');

        $this->assertNotNull($relation);
        $this->assertEquals('person', $relation->getFieldName());
        $this->assertEquals('deals', $relation->getEntity());
    }

    public function testDoesNotFindRelationByName()
    {
        $relation = ModelUtils::findRelationByFieldName($this->model, 'xxx');

        $this->assertNull($relation);
    }

    public function testFindRelationByFieldNameRecursive()
    {
        $relation = ModelUtils::findRelationByFieldNameRecursive($this->model, 'pictures');

        $this->assertNotNull($relation);
        $this->assertEquals('pictures', $relation->getFieldName());
        $this->assertEquals('persons', $relation->getEntity());
        $this->assertNull($relation->getParentRelation());
    }

    public function testFindRelationByFieldNameRecursiveNameWithDot()
    {
        $relation = ModelUtils::findRelationByFieldNameRecursive($this->model, 'person.pictures');

        $this->assertNotNull($relation);
        $this->assertEquals('pictures', $relation->getFieldName());
        $this->assertEquals('person', $relation->getEntity());

        $dealRelationOnActivity = $relation->getParentRelation();
        $this->assertEquals('person', $dealRelationOnActivity->getFieldName());
        $this->assertEquals('deals', $dealRelationOnActivity->getEntity());
    }

    public function testFindRelationByFieldNameRecursiveNameWithDotDepth3()
    {
        $relation = ModelUtils::findRelationByFieldNameRecursive(new ActivityModel(), 'deal.person.pictures');

        $this->assertNotNull($relation);
        $this->assertEquals('pictures', $relation->getFieldName());
        $this->assertEquals('person', $relation->getEntity());

        $dealRelationOnActivity = $relation->getParentRelation();
        $this->assertEquals('person', $dealRelationOnActivity->getFieldName());
        $this->assertEquals('deal', $dealRelationOnActivity->getEntity());

        $dealRelationOnActivity = $dealRelationOnActivity->getParentRelation();
        $this->assertEquals('deal', $dealRelationOnActivity->getFieldName());
        $this->assertEquals('activity', $dealRelationOnActivity->getEntity());
    }

    public function testShouldNotFindRelationByFieldNameRecursive()
    {
        $relation = ModelUtils::findRelationByFieldNameRecursive($this->model, 'xxx');

        $this->assertNull($relation);
    }

}