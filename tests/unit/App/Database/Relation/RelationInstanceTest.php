<?php

namespace Tests\unit\App\Database\Relation;


use App\Database\Relations\RelationBuilder;
use App\Database\Relations\RelationInstance;
use App\Domain\Deal\DealModel;
use PHPUnit\Framework\TestCase;

class RelationInstanceTest extends TestCase
{
    public function testGetHierarchy()
    {
        $root = RelationInstance::fromRelation($this->getRelation('root'));
        $childLevel1 = RelationInstance::fromRelation($this->getRelation('childLevel1'), $root);
        $childLevel2 = RelationInstance::fromRelation($this->getRelation('childLevel2'), $childLevel1);

        $hierarchy = $childLevel2->getHierarchy();

        $this->assertCount(3, $hierarchy);
        $this->assertEquals($root->getFieldName(), $hierarchy[0]->getFieldName());
        $this->assertEquals($childLevel1->getFieldName(), $hierarchy[1]->getFieldName());
        $this->assertEquals($childLevel2->getFieldName(), $hierarchy[2]->getFieldName());
    }

    /**
     * @return \App\Database\Relations\Relation
     */
    private function getRelation($fieldName): \App\Database\Relations\Relation
    {
        return (new RelationBuilder)
            ->setFieldName($fieldName)
            ->setEntity('')
            ->setLocalKey('id_' . $fieldName)
            ->setForeignKey('fk')
            ->setModelClass(DealModel::class)
            ->build();
    }

}