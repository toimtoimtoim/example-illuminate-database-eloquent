<?php

namespace Tests\unit\App\Field;


use App\Domain\Activity\ActivityModel;
use App\Domain\Deal\DealModel;
use App\Field\FieldService;
use App\Field\FieldValidatorService;
use App\Field\Partial\EntityFields;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class FieldValidatorServiceTest extends TestCase
{

    /**
     * @var FieldValidatorService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();

        $fieldService = $this->getMockBuilder(FieldService::class)->disableOriginalConstructor()
            ->setMethods(['getFields'])->getMock();

        $fieldService->expects($this->any())->method('getFields')->willReturn(new Collection([
            ['column' => 'id'],
            ['column' => 'subject']
        ]));

        $this->service = new FieldValidatorService($fieldService);
    }

    public function testFieldFromDb()
    {
        $fields = new EntityFields('activity');
        $fields->addField('id');

        $this->service->validate($fields, new ActivityModel([]));
        $this->assertTrue(true);
    }

    public function testFieldFromVirtualColumns()
    {
        $fields = new EntityFields('deal');
        $fields->addField('id');
        $fields->addField('now_time');

        $this->service->validate($fields, new DealModel([]));
        $this->assertTrue(true);
    }

    /**
     * @expectedException \App\AppValidationException
     * @expectedExceptionMessage field 'notexists' does not exists on entity
     */
    public function testFieldDoesNotExist()
    {
        $fields = new EntityFields('deal');
        $fields->addField('notexists');

        $this->service->validate($fields, new DealModel([]));
    }

    public function testSubEntityFieldFromVirtualColumns()
    {
        $activityFields = new EntityFields('activity');
        $activityFields->addField('id');

        $dealFields = new EntityFields('deal');
        $dealFields->addField('now_time');

        $activityFields->addSubEntity($dealFields);

        $this->service->validate($activityFields, new ActivityModel([]));
        $this->assertTrue(true);
    }

    /**
     * @expectedException \App\AppValidationException
     * @expectedExceptionMessage field 'notexists' does not exists on entity
     */
    public function testSubEntityFieldDoesNoExist()
    {
        $activityFields = new EntityFields('activity');
        $activityFields->addField('id');

        $dealFields = new EntityFields('deal');
        $dealFields->addField('notexists');

        $activityFields->addSubEntity($dealFields);

        $this->service->validate($activityFields, new ActivityModel([]));
    }

}