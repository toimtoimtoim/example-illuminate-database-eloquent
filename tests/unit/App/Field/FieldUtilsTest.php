<?php

namespace Tests\unit\App\Field;


use App\Field\FieldUtils;
use PHPUnit\Framework\TestCase;

class FieldUtilsTest extends TestCase
{
    public function testSubStringAfterLastDot()
    {
        $this->assertEquals('pictures', FieldUtils::subStringAfterLastDot('deal.person.pictures'));
        $this->assertEquals('pictures', FieldUtils::subStringAfterLastDot('person.pictures'));
        $this->assertEquals('pictures', FieldUtils::subStringAfterLastDot('pictures'));
        $this->assertEquals('', FieldUtils::subStringAfterLastDot(''));
        $this->assertNull(FieldUtils::subStringAfterLastDot(null));
    }

}