<?php

namespace Tests\unit\App\Field;

use App\Domain\Deal\DealModel;
use App\Field\FieldsToQueryPartsProcessor;
use App\Field\Partial\EntityFields;
use PHPUnit\Framework\TestCase;

class FieldsToQueryPartsProcessorTest extends TestCase
{
    public function testToQuery1LevelNesting()
    {
        $processor = new FieldsToQueryPartsProcessor();

        $fields = new EntityFields('deals');
        $fields->addField('id');

        $queryParts = $processor->process(new DealModel(), $fields);

        $selectedColumns = $queryParts->getColumns();
        $this->assertCount(1, $selectedColumns);
        $this->assertArrayHasKey('deals.id', $selectedColumns);

        $this->assertCount(0, $queryParts->getEagerLoads());
        $this->assertCount(0, $queryParts->getRelationJoins());
    }

    public function testVirtualColumn()
    {
        $processor = new FieldsToQueryPartsProcessor();

        $fields = new EntityFields('deals');
        $fields->addField('id');
        $fields->addField('now_time');

        $queryParts = $processor->process(new DealModel(), $fields);

        $selectedColumns = $queryParts->getColumns();
        $this->assertCount(2, $selectedColumns);
        $this->assertArrayHasKey('deals.id', $selectedColumns);
        $this->assertArrayHasKey('deals.now_time', $selectedColumns);

        $this->assertCount(0, $queryParts->getRelationJoins());
        $this->assertCount(0, $queryParts->getEagerLoads());
    }

    public function testToQuery2LevelNesting()
    {
        $processor = new FieldsToQueryPartsProcessor();

        $fields = new EntityFields('deals');
        $fields->addField('id');

        $orgFields = new EntityFields('organization');
        $orgFields->addField('id');
        $orgFields->addField('name');
        $fields->addSubEntity($orgFields);

        $queryParts = $processor->process(new DealModel(), $fields);

        $selectedColumns = $queryParts->getColumns();
        $this->assertCount(3, $selectedColumns);
        $this->assertArrayHasKey('deals.id', $selectedColumns);
        $this->assertArrayHasKey('organization.id', $selectedColumns);
        $this->assertArrayHasKey('organization.name', $selectedColumns);

        $relations = $queryParts->getRelationJoins();
        $this->assertCount(1, $relations);
        $this->assertEquals('organization', $relations[0]->getFieldName());

        $this->assertCount(0, $queryParts->getEagerLoads());
    }

    public function testToQuery2LevelNestingWithEagerLoad()
    {
        $processor = new FieldsToQueryPartsProcessor();

        $fields = new EntityFields('deals');
        $fields->addField('id');

        $orgFields = new EntityFields('person');
        $orgFields->addField('id');
        $orgFields->addField('pictures');
        $fields->addSubEntity($orgFields);

        $queryParts = $processor->process(new DealModel(), $fields);

        $selectedColumns = $queryParts->getColumns();
        $this->assertCount(2, $selectedColumns);
        $this->assertArrayHasKey('deals.id', $selectedColumns);
        $this->assertArrayHasKey('person.id', $selectedColumns);

        $relations = $queryParts->getRelationJoins();
        $this->assertCount(1, $relations);
        $this->assertEquals('person', $relations[0]->getFieldName());

        $eagerLoads = $queryParts->getEagerLoads();
        $this->assertCount(1, $eagerLoads);
        $this->assertEquals('pictures', $eagerLoads[0]->getRelationFieldName());
    }
}