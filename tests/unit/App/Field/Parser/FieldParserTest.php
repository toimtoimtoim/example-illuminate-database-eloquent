<?php

namespace Tests\unit\App\Field\Parser;

use App\Field\Partial\FieldParser;
use PHPUnit\Framework\TestCase;

class FieldParserTest extends TestCase
{
    public function testParse1LevelDeep()
    {
        $this->assertEquals('name,gender,owner:[name,age]',
            FieldParser::parse('name,owner(name,age),gender')->__toString()
        );
    }

    public function testParse2LevelDeep()
    {
        $this->assertEquals(
            'name,owner_id,owner:[name,picture:[id]]',
            FieldParser::parse('name,owner(name,picture(id)),owner_id')->__toString()
        );
    }

    public function testParseEmpty()
    {
        $this->assertEquals(null, FieldParser::parse(''));
    }

    public function testParseOne()
    {
        $this->assertEquals('name', FieldParser::parse('name', 'deal'));
    }

    public function testParseTwo()
    {
        $this->assertEquals('name,age', FieldParser::parse(' name, age ', 'deal')->__toString());
    }

}