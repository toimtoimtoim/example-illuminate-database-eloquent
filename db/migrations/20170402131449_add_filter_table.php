<?php

use Phinx\Migration\AbstractMigration;

class AddFilterTable extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<'EOT'
CREATE TABLE `filters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active_flag` tinyint(1) unsigned DEFAULT '1',
  `type` enum('deals','org','people','products','activity') DEFAULT 'deals',
  `temporary_flag` tinyint(1) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `visible_to` enum('1','3','5','7') NOT NULL DEFAULT '7',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `filters`');
    }
}
