<?php

use Phinx\Migration\AbstractMigration;

class AddDealFollow extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `deal_follow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `deal_id` int(11) unsigned DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_id_deal_id` (`user_id`,`deal_id`),
  KEY `idx_deal_id` (`deal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `deal_follow`');
    }
}
