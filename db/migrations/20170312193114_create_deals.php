<?php

use Phinx\Migration\AbstractMigration;

class CreateDeals extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `deals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `public_id` int(11) unsigned DEFAULT NULL,
  `company_id` int(11) unsigned DEFAULT NULL,
  `creator_user_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `person_id` int(11) unsigned DEFAULT NULL,
  `org_id` int(11) unsigned DEFAULT NULL,
  `stage_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` double NOT NULL DEFAULT '0',
  `currency` char(3) NOT NULL DEFAULT 'EUR',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `stage_change_time` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` enum('open','lost','won','deleted') DEFAULT 'open',
  `next_activity_date` date DEFAULT NULL,
  `next_activity_time` time DEFAULT NULL,
  `next_activity_id` int(11) unsigned DEFAULT NULL,
  `last_activity_id` int(11) unsigned DEFAULT NULL,
  `last_activity_date` date DEFAULT NULL,
  `lost_reason` varchar(255) DEFAULT NULL,
  `visible_to` enum('0','1','2','3','5','7') DEFAULT NULL,
  `close_time` timestamp NULL DEFAULT NULL,
  `pipeline_id` int(11) unsigned DEFAULT NULL,
  `won_time` timestamp NULL DEFAULT NULL,
  `first_won_time` timestamp NULL DEFAULT NULL,
  `lost_time` timestamp NULL DEFAULT NULL,
  `products_count` int(11) unsigned NOT NULL DEFAULT '0',
  `files_count` int(11) unsigned NOT NULL DEFAULT '0',
  `notes_count` int(11) unsigned NOT NULL DEFAULT '0',
  `followers_count` int(11) unsigned NOT NULL DEFAULT '0',
  `email_messages_count` int(11) unsigned NOT NULL DEFAULT '0',
  `activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `done_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `undone_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `reference_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `participants_count` int(11) unsigned NOT NULL DEFAULT '0',
  `expected_close_date` date DEFAULT NULL,
  `last_incoming_mail_time` timestamp NULL DEFAULT NULL,
  `last_outgoing_mail_time` timestamp NULL DEFAULT NULL,
  `ea56f96eb52f9b16ca17e79692f13359dfc94f1c` varchar(10) DEFAULT NULL,
  `52da863ca6ce9fccb2a54b1e52b625464706dafe` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_public_id` (`public_id`),
  KEY `idx_company_id` (`company_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_status` (`status`),
  KEY `idx_stage_id` (`stage_id`),
  KEY `idx_org_id` (`org_id`),
  KEY `idx_person_id` (`person_id`),
  KEY `pipeline_id` (`pipeline_id`),
  KEY `idx_lost_reason` (`lost_reason`),
  KEY `idx_active_flag` (`active`),
  KEY `idx_creator_user_id` (`creator_user_id`),
  KEY `idx_first_won_time` (`first_won_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `deals`');
    }
}
