<?php

use Phinx\Migration\AbstractMigration;

class CreateApiToken extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `api_token` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `token` varchar(255) NOT NULL,
  `active_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  UNIQUE KEY `idx_token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `api_token`');
    }
}
