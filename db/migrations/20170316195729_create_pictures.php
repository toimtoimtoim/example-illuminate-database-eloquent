<?php

use Phinx\Migration\AbstractMigration;

class CreatePictures extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `pictures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_type` enum('person','organization','product') NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `active_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `added_by_user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`added_by_user_id`),
  KEY `idx_item` (`item_id`,`item_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `pictures`');
    }
}
