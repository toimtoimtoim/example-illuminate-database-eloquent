<?php

use Phinx\Migration\AbstractMigration;

class CreateActivityParticipants extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `activity_participants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `primary_flag` tinyint(1) DEFAULT NULL COMMENT 'use 1 to indicate primary and null for not primary',
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_activity_participants` (`activity_id`,`person_id`),
  UNIQUE KEY `unq_activity_participants_primary` (`activity_id`,`primary_flag`),
  KEY `idx_person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `activity_participants`');
    }
}
