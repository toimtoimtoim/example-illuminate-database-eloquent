<?php

use Phinx\Migration\AbstractMigration;

class CreateActivities extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `done` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` char(30) NOT NULL,
  `reference_type` enum('none','email','call') NOT NULL DEFAULT 'none',
  `reference_id` int(11) unsigned DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `due_time` time DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `marked_as_done_time` timestamp NULL DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `deal_id` int(11) unsigned DEFAULT NULL,
  `org_id` int(11) unsigned DEFAULT NULL,
  `person_id` int(11) unsigned DEFAULT NULL,
  `active_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `update_time` timestamp NULL DEFAULT NULL,
  `gcal_event_id` varchar(255) DEFAULT NULL,
  `google_calendar_id` varchar(255) DEFAULT NULL,
  `google_calendar_etag` varchar(255) DEFAULT NULL,
  `writer_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `user_id` (`user_id`),
  KEY `done` (`done`),
  KEY `due_date` (`due_date`),
  KEY `type` (`type`),
  KEY `person_id` (`person_id`),
  KEY `org_id` (`org_id`),
  KEY `active_flag` (`active_flag`),
  KEY `writer_id` (`writer_id`),
  KEY `deal_id` (`deal_id`),
  KEY `gcal_event_id` (`gcal_event_id`),
  KEY `google_calendar_id` (`google_calendar_id`),
  KEY `idx_reference_id` (`reference_id`),
  KEY `idx_marked_as_done_time` (`marked_as_done_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `activities`');
    }
}
