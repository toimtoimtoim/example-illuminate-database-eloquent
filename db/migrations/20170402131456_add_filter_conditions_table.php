<?php

use Phinx\Migration\AbstractMigration;

class AddFilterConditionsTable extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<'EOT'
CREATE TABLE `filter_conditions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `operator` enum('AND','OR', '=','>','<','>=','<=','!=','LIKE ''%$%''','LIKE ''$%''','LIKE ''%$''','NOT LIKE ''%$%''','NOT LIKE ''$%''','NOT LIKE ''%$''','contains','has_been','IS NULL','IS NOT NULL','ends_at','does_not_end_at','ends_before','ends_e_before','ends_after','ends_e_after','includes','between','not_between') NOT NULL,
  `relation` varchar(255) DEFAULT NULL, -- is now 'relation' but was 'table' and type of 'enum('deals','org','people','products','activity')'
  `field_id` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `extra_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_filter_id` (`filter_id`),
  KEY `idx_field_id` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `filter_conditions`');
    }
}
