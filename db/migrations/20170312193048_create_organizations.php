<?php

use Phinx\Migration\AbstractMigration;

class CreateOrganizations extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `org` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned DEFAULT NULL,
  `owner_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `open_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_open_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `closed_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_closed_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `email_messages_count` int(11) unsigned NOT NULL DEFAULT '0',
  `people_count` int(11) unsigned NOT NULL DEFAULT '0',
  `activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `done_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `undone_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `reference_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `files_count` int(11) unsigned NOT NULL DEFAULT '0',
  `notes_count` int(11) unsigned NOT NULL DEFAULT '0',
  `followers_count` int(11) unsigned NOT NULL DEFAULT '0',
  `won_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_won_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lost_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_lost_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `active_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `category_id` int(11) unsigned DEFAULT NULL,
  `picture_id` int(11) unsigned DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `first_char` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `visible_to` enum('0','1','2','3','5','7') DEFAULT NULL,
  `next_activity_date` date DEFAULT NULL,
  `next_activity_time` time DEFAULT NULL,
  `next_activity_id` int(11) unsigned DEFAULT NULL,
  `last_activity_id` int(11) unsigned DEFAULT NULL,
  `last_activity_date` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_lat` float(10,7) DEFAULT NULL,
  `address_long` float(10,7) DEFAULT NULL,
  `address_subpremise` varchar(100) DEFAULT NULL,
  `address_street_number` varchar(100) DEFAULT NULL,
  `address_route` varchar(255) DEFAULT NULL,
  `address_sublocality` varchar(255) DEFAULT NULL,
  `address_locality` varchar(255) DEFAULT NULL,
  `address_admin_area_level_1` varchar(155) DEFAULT NULL,
  `address_admin_area_level_2` varchar(155) DEFAULT NULL,
  `address_country` varchar(155) DEFAULT NULL,
  `address_postal_code` varchar(100) DEFAULT NULL,
  `address_formatted_address` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_company_id` (`company_id`),
  KEY `idx_name` (`name`),
  KEY `idx_category_id` (`category_id`),
  KEY `idx_owner_id` (`owner_id`),
  KEY `country_code` (`country_code`),
  KEY `idx_first_char` (`first_char`),
  KEY `idx_active_flag` (`active_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `org`');
    }
}
