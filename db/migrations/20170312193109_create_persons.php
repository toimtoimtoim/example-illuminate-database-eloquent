<?php

use Phinx\Migration\AbstractMigration;

class CreatePersons extends AbstractMigration
{
    public function up()
    {
        $this->execute(<<<EOT
CREATE TABLE `persons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned DEFAULT NULL,
  `owner_id` int(11) unsigned DEFAULT NULL,
  `org_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `open_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_open_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `closed_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_closed_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `participant_open_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `participant_closed_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `email_messages_count` int(11) unsigned NOT NULL DEFAULT '0',
  `activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `done_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `undone_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `reference_activities_count` int(11) unsigned NOT NULL DEFAULT '0',
  `files_count` int(11) unsigned NOT NULL DEFAULT '0',
  `notes_count` int(11) unsigned NOT NULL DEFAULT '0',
  `followers_count` int(11) unsigned NOT NULL DEFAULT '0',
  `won_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_won_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lost_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `related_lost_deals_count` int(11) unsigned NOT NULL DEFAULT '0',
  `active_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_char` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `visible_to` enum('0','1','2','3','5','7') DEFAULT NULL,
  `google_contact_id` varchar(255) DEFAULT NULL,
  `picture_id` int(11) unsigned DEFAULT NULL,
  `sync_needed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `next_activity_date` date DEFAULT NULL,
  `next_activity_time` time DEFAULT NULL,
  `next_activity_id` int(11) DEFAULT NULL,
  `last_activity_id` int(11) DEFAULT NULL,
  `last_activity_date` date DEFAULT NULL,
  `last_incoming_mail_time` timestamp NULL DEFAULT NULL,
  `last_outgoing_mail_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_company_id` (`company_id`),
  KEY `owner_id` (`owner_id`),
  KEY `idx_first_char` (`first_char`),
  KEY `idx_org_id` (`org_id`,`name`),
  KEY `idx_name` (`name`,`org_id`),
  KEY `idx_email` (`email`),
  KEY `idx_google_contact_id` (`google_contact_id`),
  KEY `idx_active_flag` (`active_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT
EOT
        );
    }

    public function down()
    {
        $this->execute('DROP TABLE `people`');
    }
}
