<?php

use Phinx\Seed\AbstractSeed;

class CreateInitialData extends AbstractSeed
{
    // seed database with default set of data
    public function run()
    {
        $this->execute(<<<EOT
INSERT INTO `users`
(`id`,`name`,`active_flag`,`is_admin_flag`,`add_time`,`update_time`)
VALUES
('1', 'local test', '1', '1', '2017-03-02 12:38:30', '2017-03-02 11:53:02'),
('4', 'follower user', '1', '0', '2017-03-02 14:51:28', '2017-03-02 13:48:03')
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `api_token`
(`id`,`user_id`,`token`,`active_flag`,`add_time`,`update_time`)
VALUES
('1', '1', 'TOKEN_FOR_LOCAL_TEST', '1', '2017-03-02 12:38:30', '2017-03-02 11:53:02'),
('2', '1', 'TOKEN_FOR_LOCAL_TEST2', '0', '2017-03-02 12:38:30', '2017-03-02 11:53:02'),
('3', '4', 'TOKEN_FOR_FOLLOWER_USER', '1', '2017-03-02 14:51:28', '2017-03-02 13:48:03')
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `org`
(`id`,`company_id`,`owner_id`,`name`,`open_deals_count`,`related_open_deals_count`,`closed_deals_count`,`related_closed_deals_count`,
`email_messages_count`,`people_count`,`activities_count`,`done_activities_count`,`undone_activities_count`,`reference_activities_count`,
`files_count`,`notes_count`,`followers_count`,`won_deals_count`,`related_won_deals_count`,`lost_deals_count`,`related_lost_deals_count`,
`active_flag`,`category_id`,`picture_id`,`country_code`,`first_char`,`update_time`,`add_time`,`visible_to`,`next_activity_date`,`next_activity_time`,
`next_activity_id`,`last_activity_id`,`last_activity_date`,`address`,`address_lat`,`address_long`,`address_subpremise`,`address_street_number`,
`address_route`,`address_sublocality`,`address_locality`,`address_admin_area_level_1`,`address_admin_area_level_2`,`address_country`,
`address_postal_code`,`address_formatted_address`)
VALUES
('1', '4', '1', 'test company', '1', '0', '0', '0', '0', '2', '2', '0', '2', '0', '0', '0', '1', '0', '0', '0', '0', '1', NULL, NULL, NULL, 't', '2017-03-16 13:10:34', '2017-03-02 11:53:31', '3', '2017-03-16', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2', '4', '4', 'deal 2 company', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', NULL, NULL, NULL, 'd', '2017-03-02 12:44:44', '2017-03-02 11:58:55', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
)
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `persons`
(`id`,`company_id`,`owner_id`,`org_id`,`name`,`first_name`,`last_name`,`open_deals_count`,`related_open_deals_count`,`closed_deals_count`,
`related_closed_deals_count`,`participant_open_deals_count`,`participant_closed_deals_count`,`email_messages_count`,`activities_count`,
`done_activities_count`,`undone_activities_count`,`reference_activities_count`,`files_count`,`notes_count`,`followers_count`,`won_deals_count`,
`related_won_deals_count`,`lost_deals_count`,`related_lost_deals_count`,`active_flag`,`phone`,`email`,`first_char`,`update_time`,
`add_time`,`visible_to`,`google_contact_id`,`picture_id`,`sync_needed`,`next_activity_date`,`next_activity_time`,`next_activity_id`,
`last_activity_id`,`last_activity_date`,`last_incoming_mail_time`,`last_outgoing_mail_time`)
VALUES
('1', '4', '1', '1', 'uus person', 'uus', 'person', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '123', '321@eesti.ee', 'u', '2017-03-16 13:10:35', '2017-03-02 11:53:36', '3', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL
),
('2', '4', '1', '2', 'deal 2 person', 'deal', '2 person', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', NULL, NULL, 'd', '2017-03-14 12:30:00', '2017-03-02 11:59:01', '3', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL
),
('4', '4', '4', NULL, 'asd', 'asd', NULL, '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '1', NULL, NULL, 'a', '2017-03-02 14:54:12', '2017-03-02 13:50:44', '3', NULL, NULL, '0', '2017-03-02', NULL, '4', NULL, NULL, NULL, NULL)
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `deals`
(`id`,`public_id`,`company_id`,`creator_user_id`,`user_id`,`person_id`,`org_id`,`stage_id`,`title`,`value`,`currency`,
`add_time`,`update_time`,`stage_change_time`,`active`,`deleted`,`status`,`next_activity_date`,`next_activity_time`,
`next_activity_id`,`last_activity_id`,`last_activity_date`,`lost_reason`,`visible_to`,`close_time`,`pipeline_id`,
`won_time`,`first_won_time`,`lost_time`,`products_count`,`files_count`,`notes_count`,`followers_count`,`email_messages_count`,
`activities_count`,`done_activities_count`,`undone_activities_count`,`reference_activities_count`,`participants_count`,
`expected_close_date`,`last_incoming_mail_time`,`last_outgoing_mail_time`)
VALUES
('1', '119899', '4', '1', '1', '1', '1', '1', 'test company deal', '121', 'USD', '2017-03-02 11:53:44', '2017-03-16 13:32:52', NULL, '1', '0', 'open', '2017-03-16', NULL, '1', NULL, NULL, NULL, '3', NULL, '1', NULL, NULL, NULL, '0', '0', '0', '1', '0', '2', '0', '2', '0', '7', '2017-03-16', NULL, NULL
),
('2', '522162', '4', '1', '1', '2', '2', '1', 'deal 2 company deal', '12', 'USD', '2017-03-02 11:59:09', '2017-03-14 12:31:00', NULL, '1', '0', 'open', NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '1', NULL, NULL, NULL, '0', '0', '0', '1', '0', '0', '0', '0', '0', '2', '2017-03-31', NULL, NULL
),
('3', '522163', '4', '4', '4', '4', '2', '1', 'deal 3 follower user as owner', '12', 'USD', '2017-03-02 11:59:09', '2017-03-14 12:31:00', NULL, '1', '0', 'open', NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '1', NULL, NULL, NULL, '0', '0', '0', '1', '0', '0', '0', '0', '0', '2', '2017-03-31', NULL, NULL
)
EOT
        );
        $this->execute(<<<EOT
INSERT INTO `deal_follow`
(`id`, `user_id`, `deal_id`, `add_time`)
VALUES
(1, 4, 2, '2017-03-02 11:53:44')
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `activity`
(`id`,`company_id`,`user_id`,`done`,`type`,`reference_type`,`reference_id`,`due_date`,`due_time`,`duration`,`add_time`,
`marked_as_done_time`,`subject`,`deal_id`,`org_id`,`person_id`,`active_flag`,`update_time`,`gcal_event_id`,`google_calendar_id`,
`google_calendar_etag`,`writer_id`)
VALUES
('1', '4', '4', '0', 'call', 'none', NULL, '2017-03-16', NULL, NULL, '2017-03-02 12:00:59', NULL, 'test add me now !!!!!12', '1', '1', '4', '1', '2017-03-09 07:33:20', NULL, NULL, NULL, '1'
),
('2', '4', '1', '0', 'call', 'none', NULL, '2017-03-28', '12:45:00', NULL, '2017-03-02 12:30:50', NULL, 'hellou', NULL, NULL, NULL, '1', '2017-03-14 06:37:41', NULL, NULL, NULL, '1'
),
('3', '4', '1', '0', 'call', 'none', NULL, '2017-03-21', '13:30:00', NULL, '2017-03-02 12:31:26', NULL, 'mamsdmasd', NULL, NULL, '111', '1', '2017-03-16 12:52:21', NULL, NULL, NULL, '1'
),
('4', '4', '4', '0', 'call', 'none', NULL, '2017-03-02', NULL, NULL, '2017-03-02 13:50:45', NULL, 'sadsad', NULL, NULL, '4', '1', '2017-03-02 13:50:45', NULL, NULL, NULL, '4'
)
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `activity_participants`
(`id`,`activity_id`,`person_id`,`primary_flag`,`add_time`,`update_time`)
VALUES
('12', '1', '1', NULL, '2017-03-08 13:43:53', '2017-03-08 15:08:53'),
('46', '1', '2', '1', '2017-03-08 15:08:53', '2017-03-08 15:08:53'),
('47', '1', '4', NULL, '2017-03-08 15:08:53', '2017-03-08 15:08:53'),
('163', '3', '4', '1', '2017-03-16 13:09:49', '2017-03-16 13:09:49')
EOT
        );

        $this->execute(<<<EOT
INSERT INTO `pictures`
(`id`,`item_type`,`item_id`,`active_flag`,`add_time`,`update_time`,`added_by_user_id`)
VALUES
(1, 'person', '1','1', '2017-03-08 13:43:53', '2017-03-08 15:08:53', 1),
(2, 'person', '1','0', '2017-03-08 13:43:52', '2017-03-08 15:08:52', 1),
(3, 'organization', '1','1', '2017-03-08 13:43:51', '2017-03-08 15:08:51', 1)
EOT
        );

        $this->execute(<<<EOT
INSERT INTO filters (id, name, active_flag, type, temporary_flag, user_id, add_time, update_time, visible_to) 
VALUES  
('1', 'owner is 1 and deal id is 1 or 2', '1', 'deals', null, '1', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), '7'),
('2', 'person org is 2', '1', 'deals', null, '1', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), '7')
EOT
        );

        $this->execute(<<<EOT
INSERT INTO filter_conditions
(id, filter_id, parent_id, operator, `relation`, field_id, value, extra_value)
VALUES
(1, 1, null, 'OR', null, null,null, null),
(2, 1, 1, 'OR', null, null,null, null),
(3, 1, 2, '=', null, 'id', '1', null),
(4, 1, 2, '=', null, 'id', '2', null),
(5, 1, 1, '=', null, 'user_id', '1', null),
(6, 2, null, '=', 'person', 'org_id', '2', null)
EOT
        );
    }
}
